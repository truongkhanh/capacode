package counting;

public class SmallestNumberAfterDelete {
	public static String find(String input, int numDigitsToDelete){
		if (numDigitsToDelete == 0){
			return input;
		} else if (numDigitsToDelete >= input.length()){
			return "";
		}
		
		int numDigitLeft = input.length() - numDigitsToDelete;
		char[] result = new char[numDigitLeft];
		int start = 0;
		int end = 0;
		for (int i = numDigitLeft; i >= 1; i--){
			end = input.length() - i;
			int minIndex = getMinIndex(input, start, end);
			//update the digit to keep
			result[numDigitLeft - i] = input.charAt(minIndex);
			//update start for next windows
			start = minIndex + 1;
		}
		
		return String.valueOf(result);
	}
	
	private static int getMinIndex(String input, int start, int end){
		char min = input.charAt(start);
		int minIndex = start;
		for (int i = start + 1; i <= end; i++){
			//make sure compute the first (most left)
			if (input.charAt(i) < min){
				min = input.charAt(i);
				minIndex = i;
			}
		}
		
		return minIndex;
	}
}
