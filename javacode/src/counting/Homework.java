package counting;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Homework {

	public static void main(String[] args) throws FileNotFoundException{
		Scanner in = new Scanner(new File("input.txt"));
		
		int T = in.nextInt();
		for(int i = 1; i <= T; i++){
			int A = in.nextInt();
			int B = in.nextInt();
			int K = in.nextInt();
			
			System.out.println("Case #" + i + ": " + count(A, B, K)); 
			
		}
		
		in.close();
	}
	
	private static int count(int A, int B, int K){
		int [] primacity = sieveOfEratosthenes(B);
		
		int count = 0;
		for(int i = A; i <= B; i++){
			if(primacity[i] == K){
				count++;
			}
		}
		
		return count;
		
		
	}
	
	private static int[] sieveOfEratosthenes(int n){
		int[] primacity = new int[n+1];
		
		for(int i = 2; i <= n ;i++){
			if(primacity[i] == 0){
				for(int j = 1; i * j <= n; j++){
					primacity[i*j]++;
				}
			}
		}
		
		return primacity;
	}
}
