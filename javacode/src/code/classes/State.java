package code.classes;

import java.util.List;

/**
 * This class is for state in a graph
 * @author khanh
 *
 */
public class State <T>{
	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public List<T> getChildren() {
		return children;
	}

	public void setChildren(List<T> children) {
		this.children = children;
	}

	private T value;
	private List<T> children;
	
	public State(T v){
		this.value = v;
	}
	
	
}
