package code.classes;

public class SingleLinkedListNode<T> {
	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public SingleLinkedListNode<T> getNext() {
		return next;
	}

	public void setNext(SingleLinkedListNode<T> next) {
		this.next = next;
	}

	private T value;
	private SingleLinkedListNode<T> next;
	
	public SingleLinkedListNode(T value){
		this.value = value;
		next = null;
	}
}
