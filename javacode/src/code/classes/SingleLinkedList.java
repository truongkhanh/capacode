package code.classes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SingleLinkedList<T> {
	private SingleLinkedListNode<T> head;
	
	public SingleLinkedList(SingleLinkedListNode<T> head){
		this.head = head;
	}
	
	public SingleLinkedListNode<T> getHead() {
		return head;
	}

	public SingleLinkedList(T[] values){
		if(values == null || values.length == 0){
			return;
		}
		
		head = new SingleLinkedListNode<T>(values[0]);
		
		SingleLinkedListNode<T> run = head;
		for(int i = 1; i < values.length; i++){
			SingleLinkedListNode<T> newNode = new SingleLinkedListNode<T>(values[i]);
			run.setNext(newNode);
			run = newNode;
		}
		
		run.setNext(null);
	}
	
	public int length(){
		int length = 0;
		
		SingleLinkedListNode<T> run = head;
		while(run != null){
			length++;
			run = run.getNext();
		}
		
		return length;
	}
	
	public Set<T> getAllDistinctValues(){
		Set<T> result = new HashSet<T>();
		
		SingleLinkedListNode<T> run = head;
		while(run != null){
			result.add(run.getValue());
			
			run = run.getNext();
		}
		
		return result;
	}
	
	public List<T> getAllValues(){
		int length = length();
		List<T> result = new ArrayList<T>(length);
		SingleLinkedListNode<T> run = head;
		for(int i = 0; i < length; i++){
			result.add(run.getValue());
			run = run.getNext();
		}
		
		return result;
	}
	
	public boolean equals(T[] elements){
		List<T> allElements = getAllValues();
		if (allElements.size() != elements.length){
			return false;
		}
		for (int i = 0; i < allElements.size(); i++){
			if (allElements.get(i) != elements[i]){
				return false;
			}
		}
		
		return true;
		
	}
}
