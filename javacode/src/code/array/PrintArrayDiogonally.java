package code.array;

public class PrintArrayDiogonally {
	
	public static void main(String[] args){
		PrintDiogonally(new int[][]{{1,2,3,4,5}, {6,7,8,9,10}, {11,12,13,14,15}}, 3, 5);
	}
	public static void PrintDiogonally(int[][] numbers, int N, int M){
		
		for(int i = 0; i < N; i++){
			int row = i;
			int col = 0;
			while(row >= 0 && col < M){
				System.out.print(numbers[row][col] + " ");
				row--;
				col++;
			}
			System.out.println();
		}
		
		for(int j = 1; j < M; j++){
			
			int row = N-1;
			int col = j;
			while(row >= 0 && col < M){
				System.out.print(numbers[row][col] + " ");
				row--;
				col++;
			}
			System.out.println();
		}
	}
}
