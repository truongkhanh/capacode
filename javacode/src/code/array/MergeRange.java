package code.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.junit.Assert;

public class MergeRange {
	public class Range{
		public int start;
		public int end;
		public Range(int s, int e){
			start = s;
			end = e;
		}
		
		@Override
		public String toString() {
			return "[" + start + "," + end + "]";
		}
		
		@Override
	    public boolean equals(Object o) {  
	        if (o == this) {
	            return true;
	        }
	        if (!(o instanceof Range)) {
	            return false;
	        }
	        Range c = (Range) o;
	        return start == c.start && end == c.end;	         
		}
	}
	
	//compare two range by start, used to sort list of ranges by their starts
	public class RangeComparator implements Comparator<Range> {
		public int compare(Range range1, Range range2) { 
	        return Integer.compare(range1.start, range2.start);
	    }
	}
	
	public boolean hasContainedRange(List<Range> ranges){
		if (ranges.size() < 2){
			return false;
		}
		
		ranges.sort(new RangeComparator());
		for(int i = 1; i < ranges.size(); i++) {
			if (ranges.get(i).end <= ranges.get(i - 1).end){
				return true;
			}
		}
		
		return false;
	}
	
	public List<Range> merge(List<Range> ranges){
		if (ranges.size() < 2){
			return ranges;
		}
		
		ranges.sort(new RangeComparator());
		List<Range> result = new ArrayList<Range>();
		result.add(ranges.get(0));
		
		for (int i = 1; i < ranges.size(); i++){
			Range lastAdded = result.get(result.size() - 1);
			result.remove(result.size() - 1);
			List<Range> newRanges = merge(lastAdded, ranges.get(i));
			result.addAll(newRanges);			
		}
		
		return result;

	}
	
	private List<Range> merge(Range range1, Range range2){
		Assert.assertTrue(range1.start <= range2.start);
		
		if (range1.end < range2.start){
			return Arrays.asList(range1, range2);
		} else{
			Range result = new Range(range1.start, Math.max(range1.end, range2.end));
			return Arrays.asList(result);
		}
	}
	
}
