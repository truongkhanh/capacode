package code.array;


public class LongestIncreasingSubsequence {

	/**
	 * Return the longest strictly increasing subsequence
	 * O(n^2) algorithm
	 * @param A
	 * @return
	 */
	public static int[] longestIncreasingSubsequence1(int[] A) {
		int[] L = new int[A.length];
		L[0] = 1;
	
		int indexMaxLength;
		int maxLength;
		for (int u = 1; u < A.length; u++) {
			maxLength = 0;
			// search for v such that A[v] < A[u] and L[v] is max
			for (int v = 0; v < u; v++) {
				if (A[v] < A[u] && L[v] > maxLength) {
					maxLength = L[v];
				}
			}
	
			L[u] = maxLength + 1;
		}
	
		// search for longest subsequence
		indexMaxLength = 0;
		maxLength = L[indexMaxLength];
		for (int i = 1; i < A.length; i++) {
			if (L[i] > maxLength) {
				indexMaxLength = i;
				maxLength = L[i];
			}
		}
	
		// return to array
		int[] resultInArray = new int[maxLength];
		resultInArray[maxLength - 1] = A[indexMaxLength];
		
		int runInResult = resultInArray.length - 2;
		maxLength = maxLength - 1;
		for(int i = indexMaxLength-1; i>= 0 && runInResult >= 0; i--){
			if(L[i] == maxLength){
				resultInArray[runInResult] = A[i];
				runInResult--;
				
				//continue to search for increasing subsequence of length maxLength-1 for the left part
				maxLength--;
			}
		}
		return resultInArray;
	}
	
	/**
	 * Return the longest strictly increasing subsequence
	 * O(n.logn) algorithm
	 * @param A
	 * @return
	 */
	public static int[] longestIncreasingSubsequence2(int[] A) {
		
		//M[j] — stores the index k of the smallest value A[k] such that there is an increasing subsequence of length j ending at A[k] 
		int[] M = new int[A.length+1];
		M[1] = 0;
		
		//P[k] — stores the index of the predecessor of A[k] in the longest increasing subsequence ending at X[k].
		int[] P = new int[A.length];
		P[0] = -1;
		
		//init for A[0]
		int maxLengthLIS = 1;
		
		for(int k = 1; k < A.length; k++){
			//search for LIS we can extend, A[k] must be greater than
			int w = searchLargestSmaller(A, A[k], M, 1, maxLengthLIS);
			P[k] = M[w];
			
			int newLength = w + 1;
			//update maxLength of LIS
			if(newLength > maxLengthLIS){
				maxLengthLIS = newLength;
			}
			
			//update smallest element of LIS length w+1
			//A[k] will be smaller than predecessor with the same LIS
			M[newLength] = k;
		}
		
		int [] result = new int[maxLengthLIS];
		int lastIndex = M[maxLengthLIS];
		
		for(int i = maxLengthLIS - 1; i >= 0; i--){
			result[i] = A[lastIndex];
			lastIndex = P[lastIndex];
		}
		
		return result;
	}
	
	/**
	 * Search largest index i from start, end such that A[M[i]] < K
	 * Note that A[M[i]] for start <= i <= end are sorted
	 * @param A
	 * @param K
	 * @param M
	 * @param start
	 * @param end
	 * @return
	 */
	private static int searchLargestSmaller(int[] A, int K, int[] M, int start, int end){
		if(end < start || start < 0 || end > A.length - 1){
			return start-1;
		}
		
		if(A[M[end]] < K){
			return end;
		}
		else if(A[M[start]] >= K){
			return start-1;
		}
		
		int result = start-1;
		while(start <= end){
			int middle = start + (end - start)/2;
			if(A[M[middle]] < K){
				result = middle;
				//found, continue with the right part
				start = middle + 1;
			}
			else{
				end = middle - 1;
			}
		}
		
		return result;
		
	}
}
