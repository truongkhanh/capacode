package code.array;

public class SearchSortedArray {
	/*
	 * A is ascending sorted
	 * Return the smallest index of element equal K in A
	 * If not exists, return -1
	 */
	public static int search(int[] A, int K){
		return search(A, K, 0, A.length - 1);
	}
	
	/*
	 * A is ascending sorted
	 * Return the smallest index of element equal K in A
	 * If not exists, return -1
	 */
	public static int search(int[] A, int K, int start, int end){
		
		if(end < start || start < 0 || end > A.length - 1){
			return -1;
		}
		
		if(K < A[start] || A[end] < K){
			return -1;
		}
		
		while(start <= end){
			int middle = start + (end - start)/2;
			if(A[middle] == K){
				return middle;
			}
			else if(A[middle] > K){
				end = middle - 1;
			}
			else{
				start = middle + 1;
			}
		}
		
		return -1;
	}
	
	/*
	 * A is ascending sorted
	 * Return the smallest index of element equal K in A
	 * If not exists, return -1
	 */
	public static int searchSmallestIndex(int[] A, int K){
		return searchSmallestIndex(A, K, 0, A.length - 1);
	}
	
	/*
	 * A is ascending sorted
	 * Return the smallest index of element equal K in A
	 * If not exists, return -1
	 */
	public static int searchSmallestIndex(int[] A, int K, int start, int end){
		if(end < start || start < 0 || end > A.length - 1){
			return -1;
		}
		
		if(K < A[start] || A[end] < K){
			return -1;
		}
		
		int result = -1;
		while(start <= end){
			int middle = start + (end - start)/2;
			if(A[middle] == K){
				result = middle;
				//found, continue with the left part
				end = middle - 1;
			}
			else if(A[middle] > K){
				end = middle - 1;
			}
			else{
				start = middle + 1;
			}
		}
		
		return result;
	}
	
	/*
	 * A is ascending sorted
	 * Return the largest index of element equal K in A
	 * If not exists, return -1
	 */
	public static int searchLargestIndex(int[] A, int K){
		return searchLargestIndex(A, K, 0, A.length - 1);
		
	}
	/*
	 * A is ascending sorted
	 * Return the largest index of element equal K in A
	 * If not exists, return -1
	 */
	public static int searchLargestIndex(int[] A, int K, int start, int end){
		if(end < start || start < 0 || end > A.length - 1){
			return -1;
		}
		
		if(K < A[start] || A[end] < K){
			return -1;
		}
		
		int result = -1;
		while(start <= end){
			int middle = start + (end - start)/2;
			if(A[middle] == K){
				result = middle;
				//found, continue with the right part
				start = middle + 1;
			}
			else if(A[middle] > K){
				end = middle - 1;
			}
			else{
				start = middle + 1;
			}
		}
		
		return result;
	}
	
	/**
	 * Return the smallest index (smallest element) such that A[i] > K
	 * if not exists, return end+1
	 * @param A
	 * @param K
	 * @param start
	 * @param end
	 * @return
	 */
	public static int searchSmallestLarger(int[] A, int K){
		return searchSmallestLarger(A, K, 0, A.length - 1);
	}
	
	/**
	 * Return the smallest index (smallest element) such that A[i] > K
	 * if not exists, return end+1
	 * @param A
	 * @param K
	 * @param start
	 * @param end
	 * @return
	 */
	public static int searchSmallestLarger(int[] A, int K, int start, int end){
		if(end < start || start < 0 || end > A.length - 1){
			return end + 1;
		}
		
		if(A[start] > K){
			return start;
		}
		else if(A[end] <= K){
			return end+1;
		}
		
		
		int result = end+1;
		while(start <= end){
			int middle = start + (end - start)/2;
			if(A[middle] > K){
				result = middle;
				//found, continue with the left part
				end = middle - 1;
			}
			else{
				start = middle + 1;
			}
		}
		
		return result;
	}
	
	/**
	 * Return the largest index (largest element) such that A[i] < K
	 * if not exists, return start-1
	 * @param A
	 * @param K
	 * @param start
	 * @param end
	 * @return
	 */
	public static int searchLargestSmaller(int[] A, int K){
		return searchLargestSmaller(A, K, 0, A.length - 1);
		
	}
	/**
	 * Return the largest index (largest element) such that A[i] < K
	 * if not exists, return start-1
	 * @param A
	 * @param K
	 * @param start
	 * @param end
	 * @return
	 */
	public static int searchLargestSmaller(int[] A, int K, int start, int end){
		if(end < start || start < 0 || end > A.length - 1){
			return start-1;
		}
		
		if(A[end] < K){
			return end;
		}
		else if(A[start] >= K){
			return start-1;
		}
		
		int result = start-1;
		while(start <= end){
			int middle = start + (end - start)/2;
			if(A[middle] < K){
				result = middle;
				//found, continue with the right part
				start = middle + 1;
			}
			else{
				end = middle - 1;
			}
		}
		
		return result;
		
	}
	
public static int binarySearch(int[] A, int k) {
	int NOT_EXIST = -1;

	if (A.length == 0) {
		return NOT_EXIST;
	}

	int start = 0;
	int end = A.length - 1;
	//handle special case where k does not exist
	//because all elements greater or smaller than k 
	if (A[start] > k || A[end] < k) {
		return NOT_EXIST;
	}

	while (start <= end) {
		int middle = (start + end) / 2;
		if (A[middle] == k) {
			return middle;
		} else if (A[middle] > k) {
			end = middle - 1;
		} else {
			start = middle + 1;
		}
	}

	return NOT_EXIST;
}
}
