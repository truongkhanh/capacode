package code.array;

public class SortedArrayTransformation {
	
	/**
	 * Assume there is an operation change(i, v) which changes A[i] = v
	 * Return the smallest number of change to transform A to increasing sorted array
	 * @param A
	 * @return
	 */
	public static int computeNumChange(int[] A){
		return A.length - longestIncreasingSubsequence(A);
	}
	
	/**
	 * Return the length of longest strictly increasing subsequence
	 * such that for 2 continuous element in the LIS, A[u] - A[v] >= u - v
	 * O(n^2) algorithm
	 * @param A
	 * @return
	 */
	private static int longestIncreasingSubsequence(int[] A) {
		int[] L = new int[A.length];
		L[0] = 1;
	
		int maxLength;
		for (int u = 1; u < A.length; u++) {
			maxLength = 0;
			// search for v such that A[u] - A[v] >= u - v and L[v] is max
			for (int v = 0; v < u; v++) {
				if (A[u] - A[v] >= u - v && L[v] > maxLength) {
					maxLength = L[v];
				}
			}
	
			L[u] = maxLength + 1;
		}
	
		// search for longest subsequence
		maxLength = L[0];
		for (int i = 1; i < A.length; i++) {
			if (L[i] > maxLength) {
				maxLength = L[i];
			}
		}
	
		return maxLength;
	}
}
