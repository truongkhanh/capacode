package code.array;

public class SearchTwoSortedArrays {
	/**
	 * return k.th smallest elememt of union A and B where A and B are sorted
	 * @param k is from 1 to A.length + B.length
	 * @param A
	 * @param B
	 * @return
	 */
	public static int findKthSmallest(int k, int[] A, int[] B){
		//handle special case
		if(A.length == 0){
			return B[k-1];
		}
		else if(B.length == 0){
			return A[k-1];
		}
		
		//search A[k1] in A and B[k2] in B such that k1 + k2 = k
		//A[k1] >= B[k2-1] & B[k2] >= A[k1-1];
		//k1 is from 0 to A.length and k1 <= k
		
		//handle case k1 = A.length
		int lastA = A[A.length - 1];
		if(A.length + B.length == k){
			return Math.max(lastA, B[B.length - 1]);
		}
		else if(A.length <= k && B[k - A.length] >= lastA){
			return lastA;
		}
		
		//start to find k1
		int start = 0;
		int end = Math.min(A.length - 1, k);
		
		while(start <= end){
			int k1 = (start + end) /2;
			int k2 = k - k1;
			
			//over capability of B
			if(k2 > B.length){
				start = k1 + 1;
			}
			else if(k1 == 0){
				if(A[k1] >= B[k2-1]){
					return B[k2-1];
				}
				else{
					start = k1 + 1;
				}
			}
			else if(k2 == 0){
				if(B[k2] >= A[k1-1]){
					return A[k1-1];
				}
				else{
					end = k1-1;
				}
			}
			else if(k2 == B.length){
				if(A[k1] >= B[k2-1]){
					return Math.max(A[k1-1], B[k2-1]);
				}
				else if(A[k1] < B[k2-1]){
					start = k1 + 1;
				}
			}
			else{
				
				if(A[k1] >= B[k2-1] && B[k2] >= A[k1-1]){
					return Math.max(A[k1-1], B[k2-1]);
				}
				else if(A[k1] < B[k2-1]){
					start = k1 + 1;
				}
				else{//A[k1-1] > B[k2]
					end = k1 - 1;
				}
			}
		}
		
		throw new IllegalArgumentException();
		
	}
	

}
