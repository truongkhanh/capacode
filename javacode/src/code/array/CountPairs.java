package code.array;

public class CountPairs {

	public static void main(String[] args) {
		int[] numbers = new int[]{1,2,3,7};
		int count = countPair1(numbers, 8);
		System.out.println(count);
		
		count = countPair2(numbers, 8);
		System.out.println(count);
		
		//
		
		numbers = new int[]{3,4,5, 10};
		count = countPair1(numbers, 10);
		System.out.println(count);
		
		count = countPair2(numbers, 10);
		System.out.println(count);
	}

	private static int countPair1(int[] numbers, int d) {
		int count = 0;
		for (int i = 0; i < numbers.length && 2*numbers[i] <= d-1; i++) {
			int[] result = binarySearch(numbers, i + 1, numbers.length - 1, d - numbers[i] - 1);
			count += (result[1] - i);
		}

		return count;
	}

	private static int[] binarySearch(int[] numbers, int start, int end, int key) {
		int[] result = new int[] { 0, start };

		while (start <= end) {
			int middle = (end + start) / 2;
			if (numbers[middle] == key) {
				result[0] = 1;// found;
				result[1] = middle;

				// try to search the right part
				start = middle + 1;
			} else if (numbers[middle] > key) {
				end = middle - 1;
			} else {
				start = middle + 1;
			}
		}

		if (result[0] == 0) {
			//
			result[1] = end;
		}
		return result;
	}

	/*
	 * non negative numbers
	 */
	public static int countPair2(int[] numbers, int d) {
		if (numbers == null || numbers.length < 2) {
			return 0;
		}

		int length = numbers.length;
		int max = numbers[length-1];
		
		int[] maxIndex = new int[max + 1];
		maxIndex[numbers[0]] = 0;

		for (int i = 1; i < length; i++) {
			// there are hole
			if (numbers[i] != numbers[i - 1] && numbers[i] - numbers[i - 1] > 1) {
				for (int j = numbers[i - 1] + 1; j < numbers[i]; j++) {
					maxIndex[j] = maxIndex[numbers[i - 1]];
				}
			}
			maxIndex[numbers[i]] = i;
		}

		int count = 0;
		for (int i = 0; i < length && 2*numbers[i] <= d-1; i++) {
			int maxIndexComplement = 0;
			if (d - numbers[i] - 1 >= max) {
				maxIndexComplement = length - 1;
			} else {
				maxIndexComplement = maxIndex[d - numbers[i] - 1];
			}

			count += (maxIndexComplement - i);
		}

		return count;
	}
}
