package code.array;

public class SubArrayToSort {

	/***
	 * return the indicies m and n such that if we sorted the subarray from the index m to n, the entire array will be sorted
	 * minimize n - m. if the array is already sorted, return n = m = -1;
	 * @param A
	 * @return result[0] = m, result[1] = n
	 */
	public static int[] search(int[] A){
		//minSuffix[i] store the min of suffix array from i
		int[] minSuffix = new int[A.length];
		int currentMin = Integer.MAX_VALUE;
		for(int i = A.length - 1; i >= 0; i--){
			minSuffix[i] = Math.min(A[i], currentMin);
			currentMin = minSuffix[i];
		}
		
		//maxPrefix[i] store the max of prefix until i
		int[] maxPrefix = new int[A.length];
		int currentMax = Integer.MIN_VALUE;
		for(int i = 0; i < A.length; i++){
			maxPrefix[i] = Math.max(A[i], currentMax);
			currentMax = maxPrefix[i];
		}
		
		int m = 0;
		while(m < A.length && A[m] <= minSuffix[m]){
			m++;
		}
		
		if(m >= A.length){
			int[] NOT_FOUND = new int[]{-1, -1};
			return NOT_FOUND;
		}
		
		int n = A.length - 1;
		while(n >= 0 && A[n] >= maxPrefix[n]){
			n--;
		}
		
		return new int[]{m, n};
	}
}
