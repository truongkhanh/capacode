package code.array;

//Problem 1: An array n unique numbers in the range from 0 to n.
//There is only one number in the range from 0 to n missing.
public class FindMissingNumber {
	public static int find1(int A[]){
		int N = A.length;
		int sumAll = (N * (N+1)) / 2;
		int sumA = 0;
		for (int num: A){
			sumA += num;
		}
		
		return sumAll - sumA;
	}
	
	public static int find2(int A[]){
		int N = A.length;
		
		int xorResult = 0;
		for (int i = 0; i <= N; i++){
			xorResult ^= i;
		}
		for (int num: A){
			xorResult ^= num;
		}
		
		return xorResult;
	}
}
