package code.array;

public class SearchIndexEqualValue {

	/**
	 * A is ascending sorted of distinct integer.
	 * Search for A[i] = i
	 * @param A
	 * @return i
	 */
public static int search(int[] A){
	if(A.length == 0){
		return -1;
	}
	
	int start = 0;
	int end = A.length - 1;
	
	if(A[start] - start > 0 || A[end] - end < 0){
		return -1;
	}
	
	
	while(start <= end){
		int middle = start + (end - start)/2;
		if(A[middle] == middle){
			return middle;
		}
		else if(A[middle] > middle){
			end = middle - 1;
		}
		else{
			start = middle + 1;
		}
	}
	
	return -1;
}
}
