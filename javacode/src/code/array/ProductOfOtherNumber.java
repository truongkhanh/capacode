package code.array;

public class ProductOfOtherNumber {
	public static int[] productOfOtherNumbers(int [] A)
	{
		int product = 1;
		int numberOfZeroElements = 0;
		int firstZeroElementIndex = -1;
		for (int i = 0; i < A.length; i++)
		{
			
			if (A[i] == 0){
				numberOfZeroElements ++;
				firstZeroElementIndex = i;
				if (numberOfZeroElements == 2)
				{
					break;
				}
			}
			else
			{
				product *= A[i];
			}
		}
		int[] result = new int[A.length];
		if (numberOfZeroElements == 1)
		{
			result[firstZeroElementIndex] =  product;
		}
		else if (numberOfZeroElements == 0)
		{
			for (int i = 0; i < A.length; i++)
			{
				result[i] = product / A[i];
			}
		}
		return result;
	}
	
public static int[] productOfOtherNumbersNoDivision(int [] A)
{
	int[] C = new int[A.length];
	C[0] = 1;
	for (int i = 1; i < C.length; i++)
	{
		C[i] = A[i-1] * C[i-1];
	}
	
	int[] D = new int[A.length];
	D[D.length - 1] = 1;
	for (int i = D.length - 2; i >= 0; i--)
	{
		D[i] = A[i+1] * D[i+1];
	}
	
	//use C as returned array
	for (int i = 0; i < C.length; i++)
	{
		C[i] *= D[i];
	}
	
	return C;
}
}
