package code.array;

import java.util.Arrays;

public class CookingtheBooks {
	

	public static char[] getSmallestNumberCanChange(char[] number){
		int numberOfDigits = number.length;
		
		char[] result = Arrays.copyOf(number, numberOfDigits);
		
		int minIndex = computeNotZeroMin(result);
		
		if(minIndex != -1 && number[0] > number[minIndex]){
			swap(result, 0, minIndex);
			return result;
		}
		
		//min[i] is the min digit of digits from i to end
		char[] min = new char[numberOfDigits];
		
		//index[i] is the index of min[i], take largest index as possible
		int[] index = new int[numberOfDigits];
		
		//init with suffix from numberOfDigits -1
		int lastIndex = numberOfDigits - 1;
		min[lastIndex] = result[lastIndex];
		index[lastIndex] = lastIndex;
		
		
		for(int i = numberOfDigits - 2; i >= 0; i--){
			computeMinSuffix(result, min, index, i);
		}
		
		//swap from the second digit
		for(int i = 1; i < numberOfDigits; i++){
			if(result[i] > min[i]){
				swap(result, i, index[i]);
				break;
			}
		}
		
		return result;
	
	}
	
	static char ZERO = '0';
	static char MAX = 127;
	
	
	/**
	 * Return largest index of non zero min
	 * @param result
	 * @return
	 */
	private static int computeNotZeroMin(char[] result) {
		char min = MAX;
		int index = -1;
	
		for (int i = 1; i < result.length; i++) {
			if (result[i] != ZERO && result[i] <= min) {
				min = result[i];
				index = i;
			}
		}
		return index;
		
	}
	
	/**
	 * Make sure that minIndexes as largest as possible
	 */
	private static void computeMinSuffix(char[] number, char[] min, int[] index, int i) {
		
		if(number[i] < min[i+1]){
			min[i] = number[i];
			index[i] = i;
		}
		else{
			min[i] = min[i+1];
			index[i] = index[i+1];
		}
	}
	
	
	private static void swap(char[] number, int i, int j){
		char temp = number[i];
		number[i] = number[j];
		number[j] = temp;
	}
}
