package code.array;

import java.util.Arrays;

/**
 * Check whether there exists 1 pair of elements in an array whose sum is k
 * @author khanh
 *
 */
public class PairWithSum {
	public static boolean havingPairWithSum(int[] A, int k){
		Arrays.sort(A);
		
		int start = 0;
		int end = A.length - 1;
		while(start < end){
			int sum = A[start] + A[end];
			
			if(sum == k){
				return true;
			}
			else if(sum < k){
				start++;
			}
			else{
				end--;
			}
		}
		
		return false;
	}
}
