package code.dynamicprogramming;


public class MoneyChange {
	public static int numWaysChange(int amount, int[] notevalues){
		if(amount < 0){
			return 0;
		}
		
		int[][] waysToChange = new int[amount+1][notevalues.length];
		for(int i = 0; i < notevalues.length; i++){
			//0 dollar is changed with no note
			waysToChange[0][i] = 1;
		}
		
		for(int i = 1; i <= amount; i++){
			//assume there is only one type of note
			//it is able to change if the amount is divisible by the note value
			waysToChange[i][0] = (i % notevalues[0] == 0)? 1: 0;
		}
		
		for(int i = 1; i <= amount; i++){
			for(int j = 1; j < notevalues.length; j++){
				waysToChange[i][j] = waysToChange[i][j-1];
				if(i >= notevalues[j]){
					waysToChange[i][j] += waysToChange[i-notevalues[j]][j];
				}
			}
		}
		
		return waysToChange[amount][notevalues.length - 1];
		
	}
}
