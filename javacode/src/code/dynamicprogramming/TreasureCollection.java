package code.dynamicprogramming;

public class TreasureCollection {

public static int computeMaxTreasures(int[][] map){
	int numRows = map.length;
	if(numRows == 0){
		return 0;
	}
	
	int numCols = map[0].length;
	if(numCols == 0){
		return 0;
	}
	
	//
	int [][] maxTreasures = new int[numRows][numCols];
	//count for starting at last row
	int maxTreasureOfLastCell = 0;
	for(int i = numCols - 1; i >= 0; i--){
		maxTreasures[numRows-1][i] = maxTreasureOfLastCell + map[numRows-1][i];
		maxTreasureOfLastCell = maxTreasures[numRows-1][i];
	}
	
	//count for starting at last column
	maxTreasureOfLastCell = 0;
	for(int i = numRows - 1; i >= 0; i--){
		maxTreasures[i][numCols-1] = maxTreasureOfLastCell + map[i][numCols - 1];
	}
	
	for(int row = numRows - 2; row >= 0; row--){
		for(int col = numCols - 2; col >= 0; col--){
			int max = Math.max(maxTreasures[row][col + 1], maxTreasures[row + 1][col]);
			maxTreasures[row][col] = max + map[row][col];
		}
	}
	
	return maxTreasures[0][0];
}
	
	public static void main(String[] args){
		int [][] map = new int[][]{{1,10, 3, 8}, {12, 2, 9, 6}, {5, 7, 4, 11}, {3, 7, 16, 5}};
		System.out.println(computeMaxTreasures(map));
	}
}


