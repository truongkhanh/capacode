package code.recursion;

public class Permutation {
	public static void permutationNotOrder(char[] s){
		permutationNotOrder(s, 0);
	}
	private static void permutationNotOrder(char[] s, int start){
		if(start == s.length){
			System.out.println(s);
			return;
		}
		
		for(int i = start; i < s.length; i++){
			swap(s, start, i);
			
			permutationNotOrder(s, start+1);
			
			swap(s, start, i);
		}
		
		
	}
	
	public static void permutationNotOrderNoDuplication(char[] s){
		permutationNotOrderNoDuplication(s, 0);
	}
	private static void permutationNotOrderNoDuplication(char[] s, int start){
		if(start == s.length){
			System.out.println(s);
			return;
		}
		
		for(int i = start; i < s.length; i++){
			if(!contains(s, start, i-1, s[i])){
				swap(s, start, i);
				
				permutationNotOrderNoDuplication(s, start+1);
				
				swap(s, start, i);
			}
		}
		
		
	}
	
	private static void swap(char[] s, int i, int j){
		char c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
	
	private static boolean contains(char[] arr, int start, int end, int value) {
		for (int i = start; i <= end; i++) {
			if (arr[i] == value) {
				return true;
			}
		}
		return false;
	}
	
	public  static void permutationInOrder(String s) { 
		permutationInOrder("", s); 
	}
	
    private static void permutationInOrder(String prefix, String s) {
        int N = s.length();
        if (N == 0) System.out.println(prefix);
        else {
            for (int i = 0; i < N; i++){
            	permutationInOrder(prefix + s.charAt(i), s.substring(0, i) + s.substring(i+1, N));
            }
        }

    }

	
	public static void main(String[] args){
		permutationInOrder("abc");
	}
}
