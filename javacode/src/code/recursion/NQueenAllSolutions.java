package code.recursion;

import java.util.ArrayList;
import java.util.List;

public class NQueenAllSolutions {

	/**
	 * Store all solutions
	 */
	List<int[]> result = new ArrayList<int[]>();
	
	public List<int[]> solve(int size) {
		result.clear();
		search(0, new int[size], size);
		return result;
	}

	private void search(int rowIndex, int[] positionsOnRows, int N) {
		if (rowIndex == N) {
			if (isASolution(positionsOnRows)) {
				result.add(positionsOnRows.clone());

			}
		} else {
			// try all possible column for this row
			for (int i = 0; i < N; i++) {
				positionsOnRows[rowIndex] = i;
				search(rowIndex + 1, positionsOnRows, N);
			}
		}
	}
	
	

	public boolean isASolution(int[] positionsOnRows) {
		for (int i = 0; i < positionsOnRows.length; i++) {
			for (int j = i + 1; j < positionsOnRows.length; j++) {
				if (positionsOnRows[i] == positionsOnRows[j]) {
					// 2 queens same column
					return false;
				} else if (j - i == Math.abs(positionsOnRows[j]
						- positionsOnRows[i])) {
					// 2 queens on diagonal line
					return false;
				}
			}
		}

		return true;
	}


	private void searchWithPruning(int rowIndex, int[] positionsOnRows, int size) {
		if (rowIndex == size) {
			result.add(positionsOnRows);
		} else {
			// try all possible column for this row
			for (int i = 0; i < size; i++) {
				positionsOnRows[rowIndex] = i;
				if (isSafe(rowIndex, positionsOnRows)) {
					search(rowIndex + 1, positionsOnRows, size);
				}
			}
		}
	}

	/**
	 * Just check the latest queen is safe
	 */
	private boolean isSafe(int lastRow, int[] positionsOnRows) {
		for (int i = 0; i < lastRow; i++) {
			if (positionsOnRows[i] == positionsOnRows[lastRow]) {
				return false;
			} else if (lastRow - i == Math.abs(positionsOnRows[lastRow]
					- positionsOnRows[i])) {
				return false;
			}
		}

		return true;
	}
}
