package code.recursion;

public class NQueenSolutionCounting {

	private int count = 0;
	private int size;
	
	private boolean[] isRowSafe;
	private boolean[] isColumnSafe;
	private boolean[] isDiagonalRightLeftSafe;
	private boolean[] isDiagonalLeftRightSafe;
	
	public int solve(int size) {
		initData(size);
		searchWithPruning(0);
		return count;
	}
	
	private void initData(int size) {
		this.size = size;
		this.count = 0;
		isRowSafe = new boolean[size];
		isColumnSafe = new boolean[size];
		isDiagonalLeftRightSafe = new boolean[size*2];
		isDiagonalRightLeftSafe = new boolean[size*2];
		
		for(int i = 0; i < size; i++){
			isRowSafe[i] = true;
			isColumnSafe[i] = true;
		}
		
		for(int i = 0; i < size * 2; i++){
			isDiagonalLeftRightSafe[i] = true;
			isDiagonalRightLeftSafe[i] = true;
		}
	}
	
	private void searchWithPruning(int rowIndex) {
		if (rowIndex == size) {
			count++;
		} else {
			// try all possible column for this row
			for (int columnIndex = 0; columnIndex < size; columnIndex++) {
				if (isRowSafe[rowIndex] && isColumnSafe[columnIndex] &&
						isDiagonalRightLeftSafe[rowIndex + columnIndex] && isDiagonalLeftRightSafe[size - 1 + rowIndex - columnIndex]) {
					
					setValue(rowIndex, columnIndex, false);
					searchWithPruning(rowIndex + 1);
					setValue(rowIndex, columnIndex, true);
					
				}
			}
		}
	}
	
	private void setValue(int rowIndex, int columnIndex, boolean value){
		isRowSafe[rowIndex] = value;
		isColumnSafe[columnIndex] = value;
		isDiagonalRightLeftSafe[rowIndex + columnIndex] = value;
		isDiagonalLeftRightSafe[size - 1 + rowIndex - columnIndex] = value;
	}
}
