package code.recursion;

import org.junit.Assert;
import org.junit.Test;

public class Fibonacci {

	public static int DP(int n){
		if(n < 2){
			return n;
		}
		int[] result = new int[n+1];
		result[0] = 0;
		result[1] = 1;
		for(int i = 2; i <= n; i++){
			result[i] = result[i-1] + result[i-2];
		}
		
		return result[n];
	}
	public static int recursion(int n){
		if(n < 2){
			return n;
		}
		else{
			return recursion(n-1) + recursion(n-2);
		}
	}
	
public static int DP1(int n){
	if(n < 2){
		return n;
	}
	int n1 = 0;
	int n2 = 1;
	for(int i = 2; i <= n; i++){
		int sum = n1 + n2;
		n1 = n2;
		n2 = sum;
	}
	
	return n2;
}
	@Test
	public void test1(){
		int n = 3;
		int expected = 2;
		int result = recursion(n);
		
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void test2(){
		int n = 45;
		int expected = 1134903170;
		int result = DP1(n);
		
		Assert.assertEquals(expected, result);
	}
}
