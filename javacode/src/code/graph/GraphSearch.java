package code.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

public class GraphSearch {
	
	/**
     * Return the edges from root to a state in goal. return null if unreachable
     * @param root
     * @param goal
     * @return
     */
    public static List<Edge> DFS(int root, Set<Integer> goal, Graph graph){
		//store the path from root to current Node
		List<Edge> path = new ArrayList<Edge>();
		//for each node in path, store its depth level
		List<Integer> depthList = new ArrayList<Integer>();

		//store nodes waiting to visit
		Stack<Edge> workingStates = new Stack<Edge>();
		Edge dummyEdge = new Edge(0, root);
		workingStates.push(dummyEdge);

		//for each node in workingStates, store its depth level
		Stack<Integer> depthStack = new Stack<Integer>();
		depthStack.push(0);

		//check whether a node is visited or not
		boolean [] isVisited = new boolean[graph.V()];
		isVisited[root] = true;
		while(!workingStates.isEmpty()){
			Edge currentEdge = workingStates.pop();
			int currentState = currentEdge.to();
			int depthLevel = depthStack.pop();

			while(depthList.size() > 0){
					int lastDepth = depthList.get(depthList.size() - 1);
					if(lastDepth >= depthLevel){
						//back track a new node, remove nodes not in the path to this node (having depth level greater than or equal its depth level
						depthList.remove(depthList.size() - 1);
						path.remove(path.size() - 1);
					}
					else{
						break;
					}
			}
			
			//add this node and its depth level
			path.add(currentEdge);
			depthList.add(depthLevel);

			//check reachable
			if(goal.contains(currentState)){
				//remove dummy
				path.remove(0);
				return path;
			}

			//add new states to workingState
			for(Edge edge: graph.adj(currentState)){
				if(!isVisited[edge.to()]){
					workingStates.push(edge);
					depthStack.push(depthLevel+1);
					
					isVisited[edge.to()] = true;
				}
			}
		}
		
		return null;

	}
	
    /**
     * Check whether a state in goal reachable from root
     * @param root
     * @param goal
     * @param graph
     * @return
     */
	public static boolean isReachableDFS(int root, Set<Integer> goal, Graph graph){
		//store nodes waiting to visit
		Stack<Integer> workingStates = new Stack<Integer>();
		workingStates.push(root);

		//check whether a node is visited or not
		boolean [] isVisited = new boolean[graph.V()];
		isVisited[root] = true;
		while(!workingStates.isEmpty()){
			int currentState = workingStates.pop();
			

			//check reachable
			if(goal.contains(currentState)){
				return true;
			}

			//add new states to workingState
			for(Edge edge: graph.adj(currentState)){
				if(!isVisited[edge.to()]){
					workingStates.push(edge.to());
					
					isVisited[edge.to()] = true;
				}
			}
		}
		
		return false;

	}
}
