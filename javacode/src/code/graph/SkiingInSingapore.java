package code.graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class SkiingInSingapore {
	private class State{
		private int index;
		private int value;
		private List<Integer> next;
		
		public State(int index, int v){
			this.index = index;
			this.value = v;
			next = new ArrayList<Integer>();
		}
		
		public void addNext(int child){
			next.add(child);
		}
		
		public int getValue(){
			return value;
		}

		public int getIndex() {
			return index;
		}

		public List<Integer> getNext() {
			return next;
		}
		
		@Override
		public String toString() {
			return index + "->" + next.toString();
		}
	}
	
	private List<State> states = new ArrayList<>();
	private int numRows = 0;
	private int numCols = 0;
	
	private int maxLength = Integer.MIN_VALUE;
	private int maxSteep = Integer.MIN_VALUE;
	
	public String longestSteepestPath(String inputFile) throws FileNotFoundException{
		processInput(inputFile);
		boolean [] needToTry = new boolean[states.size()];
		for (int i = 0; i < needToTry.length; i++){
			needToTry[i] = true;
		}
		
		for (State start: states){
			if (needToTry[start.getIndex()]){
				tryStartAt(start, needToTry);
			}
		}
		
		return String.valueOf(maxLength) + String.valueOf(maxSteep);
	}
	
	//graph has no cycle
	private void tryStartAt(State start, boolean[] needToTry){
		//store states waiting to visit
		Stack<State> workingStates = new Stack<State>();
		workingStates.push(start);

		//for each state in workingStates, store its depth level
		Stack<Integer> depthStack = new Stack<Integer>();
		depthStack.push(1);

		while(!workingStates.isEmpty()){
			State currentState = workingStates.pop();
			int depthLevel = depthStack.pop();
			
			//can not go further
			if (currentState.getNext().size() == 0){
				int steep = start.getValue() - currentState.getValue();
				if (depthLevel > maxLength || (depthLevel == maxLength && steep > maxSteep)){
					maxLength = depthLevel;
					maxSteep = steep;
				}
			}
			
			//add new states to workingState
			for(int next: currentState.getNext()){
				//starting from "next" is never better than "start"
				State nextState = states.get(next);
				needToTry[nextState.getIndex()] = false;
				workingStates.push(nextState);
				depthStack.push(depthLevel + 1);
			}
		}
	}
	private void processInput(String inputFile) throws FileNotFoundException{
		int[][] matrix = readFile(inputFile);
		buildStates(matrix);
	}
	
	private int[][] readFile(String input) throws FileNotFoundException{
		Scanner in = new Scanner(new File(input));
		numRows = in.nextInt();
		numCols = in.nextInt();
		
		int[][] matrix = new int[numRows][numCols];
		for (int i = 0; i < numRows; i++){
			for (int j = 0; j < numCols; j++){
				matrix[i][j] = in.nextInt();
			}
		}
		in.close();
		return matrix;
	}
	
	private void buildStates(int[][] matrix){
		for (int i = 0; i < numRows; i++){
			for (int j = 0; j < numCols; j++){
				State newState = new State(getStateIndex(i, j), matrix[i][j]);
				if (i > 0 && matrix[i][j] > matrix[i-1][j]){
					newState.addNext(getStateIndex(i - 1, j));
				}
				
				if (i < numRows - 1 && matrix[i][j] > matrix[i+1][j]){
					newState.addNext(getStateIndex(i + 1, j));
				}
				
				if (j > 0 && matrix[i][j] > matrix[i][j-1]){
					newState.addNext(getStateIndex(i, j - 1));
				}
				
				if (j < numCols - 1 && matrix[i][j] > matrix[i][j+1]){
					newState.addNext(getStateIndex(i, j + 1));
				}
				states.add(newState);
			}
		}
	}
	
	private int getStateIndex(int rowIndex, int colIndex){
		return rowIndex * numCols + colIndex;
	}
}
