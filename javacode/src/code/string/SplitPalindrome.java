package code.string;

public class SplitPalindrome {
	public static int split(String s)
	{	
		//numberOfSplits[i] is the minimum number of splits for substring starting at s[i]
		int [] numberOfSplits = new int[s.length()];
		numberOfSplits[s.length() - 1] = 0;
		
		for (int i = s.length() - 2; i >= 0; i--)
		{
			//for each new char s[i], it is belong to a palindrome starting at s[i] or it is a new palindrome length 1
			int minSplit = Integer.MAX_VALUE;
			for (int j = i; j < s.length(); j++)
			{
				if (Palindrome.isPalindrome(s.substring(i, j + 1)))
				{
					int numSplits = (j + 1 < s.length())? 1 + numberOfSplits[j + 1]: 0;
					if (numSplits < minSplit)
					{
						minSplit = numSplits;
					}
				}
			}
			numberOfSplits[i] = minSplit;
		}
		
		return numberOfSplits[0];
	}
}
