package code.string;

/**
 * Search for the first unique character in a string
 * @author khanh
 *
 */
public class FirstUniqueChar {
	public static int firstUniqueChar(String s){
		int NOT_APPEAR = -1;
		int DUPLICATE = -2;
		
		int[] position = new int[256];
		for(int i = 0; i < position.length; i++){
			position[i] = NOT_APPEAR ;
		}

		for(int i = 0; i < s.length(); i++){
			//first appearing
			if(position[s.charAt(i)] == NOT_APPEAR ){
				position[s.charAt(i)] = i;
			}
			else if(position[s.charAt(i)] >= 0){
				//duplicate
				position[s.charAt(i)] = DUPLICATE;
			}
		}

		int firstUniqueChar = Integer.MAX_VALUE;
		for(int i = 0; i < position.length; i++){
			if(position[i] >= 0 && position[i] < firstUniqueChar){
				firstUniqueChar = position[i];
			}
		}
		
		return (firstUniqueChar != Integer.MAX_VALUE)? firstUniqueChar: -1;
	}
}
