package code.string;

import java.util.HashMap;
import java.util.Map;

public class PatternMatching {
	/*
	 * pattern is a sequence of english letter input is a string pattern "abba"
	 * , string "redbluebluered" matched definition of each character must be
	 * distinct each definition at least one character
	 */
	public static boolean isMatched(String pattern, String input) {
		return isMatchedRecur(pattern.toCharArray(), 0, input.toCharArray(), 0,
				new HashMap<Character, String>());
	
	}
	
	/**
	 * Check whether input matches pattern with the current definition
	 * @param pattern
	 * @param startPattern
	 * @param input
	 * @param startInput
	 * @param defition
	 * @return
	 */
	private static boolean isMatchedRecur(char[] pattern, int startPattern,
			char[] input, int startInput, Map<Character, String> defition) {
		boolean finishedPattern = startPattern >= pattern.length;
		boolean finishedInput = startInput >= input.length;
		if (finishedInput && finishedPattern) {
			return true;
		} else if (finishedInput ^ finishedPattern) {
			return false;
		}
	
		char nextPatternChar = pattern[startPattern];
	
		//if the next character in pattern is already defined, check its definition as prefix of the input
		if (defition.containsKey(nextPatternChar)) {
			String charDef = defition.get(nextPatternChar);
	
			if (startInput + charDef.length() - 1 >= input.length) {
				return false;
			}
	
			for (int i = 0; i < charDef.length(); i++) {
				if (charDef.charAt(i) != input[startInput + i]) {
					return false;
				}
			}
	
			return isMatchedRecur(pattern, startPattern + 1, input, startInput
					+ charDef.length(), defition);
		} else {
			StringBuilder builder = new StringBuilder();
			
			//try all possible definition of the current character in the pattern
			for (int i = startInput; i < input.length; i++) {
				builder.append(input[i]);
	
				String newDef = builder.toString();
	
				//make sure the definition is distinct
				if (!defition.containsValue(newDef)) {
					defition.put(nextPatternChar, newDef);
	
					boolean isMatch = isMatchedRecur(pattern, startPattern + 1,
							input, startInput + newDef.length(), defition);
					if (isMatch) {
						return true;
					}
	
					//
					defition.remove(nextPatternChar);
				}
			}
	
			return false;
		}
	}
}
