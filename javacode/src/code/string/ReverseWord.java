package code.string;

/**
 * Reverse word in a string
 * For example "coding is fun" -> "fun is coding"
 * @author khanh
 *
 */
public class ReverseWord {
	private static char SPACE = ' ';
	public static String reverseWord(String s){
		char [] sAsArray = s.toCharArray();
		
		//reverse the whole string
		reverse(sAsArray, 0, sAsArray.length-1);
		
		int start = 0;
		while(start < sAsArray.length){
			//first char of word
			for(; start < sAsArray.length && sAsArray[start] == SPACE; start++);
			
			if(start < sAsArray.length){
				//last char of word
				int end = start + 1;
				for(; end < sAsArray.length && sAsArray[end] != SPACE; end++);
				
				//reverse new word
				reverse(sAsArray, start, end - 1);
				start = end;
			}
		}
		
		return new String(sAsArray);
	}
	
	/**
	 * reverse sub string of s from start to end
	 * @param s
	 * @param start
	 * @param end
	 */
	private static void reverse(char[] s, int start, int end){
		while(start < end){
			swap(s, start, end);
			start++;
			end--;
		}
	}
	
	private static void swap(char[] s, int i, int j){
		char c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}
