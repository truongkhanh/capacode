package code.string;

public class Palindrome {

	public static boolean isPalindrome(String s) {
		int start = 0;
		int end = s.length() - 1;

		while (start < end) {
			if (s.charAt(start) != s.charAt(end)) {
				return false;
			}
			start++;
			end--;
		}

		return true;
	}

	public static int longestPalindrome(String s) {

		int maxLength = 0;

		// center at s[i]
		for (int i = 0; i < s.length(); i++) {
			int lengthCanExtend = (s.length() - i - 1) * 2 + 1;

			if (lengthCanExtend > maxLength) {
				int count = 1;
				int start = i - 1;
				int end = i + 1;
				while (start >= 0 && end < s.length()
						&& s.charAt(start) == s.charAt(end)) {
					count += 2;
					start--;
					end++;
				}
				if (count > maxLength) {
					maxLength = count;
				}
			} else {
				// if at i, we can not extend to obtain better than maxLength,
				// exit
				break;
			}
		}

		// center at s[i] and s[i+1]
		for (int i = 0; i < s.length() - 1; i++) {
			// max of extending to right
			int lengthCanExtend = (s.length() - i - 1) * 2;

			if (lengthCanExtend > maxLength) {
				int count = 0;
				int start = i;
				int end = i + 1;

				while (start >= 0 && end < s.length()
						&& s.charAt(start) == s.charAt(end)) {
					count += 2;
					start--;
					end++;
				}
				if (count > maxLength) {
					maxLength = count;
				}
			} else {
				// if at i, we can not extend to obtain better than maxLength,
				// exit
				break;
			}
		}

		return maxLength;

	}

	public static int longestPrefixPalindrome(String s) {
		int maxLength = 0;

		// center at s[i]
		for (int i = 0; i < s.length(); i++) {
			int lengthCanExtend = (s.length() - i - 1) * 2 + 1;

			if (lengthCanExtend > maxLength) {
				int count = 1;
				int start = i - 1;
				int end = i + 1;
				while (start >= 0 && end < s.length()
						&& s.charAt(start) == s.charAt(end)) {
					count += 2;
					start--;
					end++;
				}
				// make sure palindrome is prefix
				if (start < 0 && count > maxLength) {
					maxLength = count;
				}
			} else {
				// if at i, we can not extend to obtain better than maxLength,
				// exit
				break;
			}
		}

		// center at s[i] and s[i+1]
		for (int i = 0; i < s.length() - 1; i++) {
			// max of extending to right
			int lengthCanExtend = (s.length() - i - 1) * 2;

			if (lengthCanExtend > maxLength) {
				int count = 0;
				int start = i;
				int end = i + 1;

				while (start >= 0 && end < s.length()
						&& s.charAt(start) == s.charAt(end)) {
					count += 2;
					start--;
					end++;
				}
				// make sure palindrome is prefix
				if (start < 0 && count > maxLength) {
					maxLength = count;
				}
			} else {
				// if at i, we can not extend to obtain better than maxLength,
				// exit
				break;
			}
		}

		return maxLength;
	}

	public static int longestPalindromeDP(String s) {
		if (s == null || s.length() == 0) {
			return 0;
		}

		int maxLength = 1;
		// isPalindrome[i][j] if substring from i to j (both inclusive) is
		// palindrome
		boolean[][] isPalinDrome = new boolean[s.length()][s.length()];
		for (int i = 0; i < s.length(); i++) {
			isPalinDrome[i][i] = true;

			if (i < s.length() - 1) {
				isPalinDrome[i][i + 1] = (s.charAt(i) == s.charAt(i + 1));
				maxLength = 2;
			}
		}

		for (int d = 2; d < s.length(); d++) {
			for (int i = 0; i + d < s.length(); i++) {
				int j = i + d;
				isPalinDrome[i][j] = ((s.charAt(i) == s.charAt(j)) && isPalinDrome[i + 1][j - 1]);
				if (isPalinDrome[i][j] && d + 1 > maxLength) {
					maxLength = d + 1;
				}
			}
		}

		return maxLength;

	}
}
