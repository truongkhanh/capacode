package code.string;

/**
 * Check 2 strings are anagram
 * We can rearrange one string to have the other string
 * @author khanh
 *
 */
public class Anagram {
	static char ZCHAR = 'z';
	static char ACHAR = 'a';
	public static boolean isAnagram(String s1, String s2){
		if(s1.length() != s2.length()){
			return false;
		}
		
		int[] countChars = new int[ZCHAR - ACHAR + 1];
		
		for(int i = 0; i < s1.length(); i++){
			char nextChar = s1.charAt(i);
			
			countChars[nextChar - ACHAR]++;
		}
		
		for(int i = 0; i < s2.length(); i++){
			char nextChar = s2.charAt(i);
			countChars[nextChar - ACHAR]--;
			
			if(countChars[nextChar - ACHAR] < 0){
				return false;
			}
		}
		
		for(int i = 0; i < countChars.length; i++){
			if(countChars[i] != 0){
				return false;
			}
		}
		
		return true;
	}
}
