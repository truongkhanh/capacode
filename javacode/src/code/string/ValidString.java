package code.string;

import java.util.Set;

public class ValidString {
	public static boolean isValidString(Set<String> dictionary, String s){
		//compute shortest, longest word
		int minLength = Integer.MAX_VALUE;
		int maxLength = Integer.MIN_VALUE;
		for(String word: dictionary){
			if(word.length() > maxLength){
				maxLength = word.length();
			}
			
			if(word.length() < minLength){
				minLength = word.length();
			}
		}
		
		int length = s.length();
		
		boolean[] valid = new boolean[length+1];
		valid[length] = true;
	
		for(int u = length - 1; u >= 0; u--){
			//check substring length within minLength and maxLength
			for(int v = u + minLength; v <= s.length() && v <= u + maxLength; v++){
				if(dictionary.contains(s.substring(u, v)) && valid[v]){
					valid[u] = true;
					break;
				}
			}
		}
	
		return valid[0];
	
	}
}
