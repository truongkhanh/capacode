package code.linkedlist;

import code.classes.SingleLinkedListNode;

public class RevertLinkedList {
	public static SingleLinkedListNode<Integer> revert(SingleLinkedListNode<Integer> head){
		if (head == null || head.getNext() == null){
			return head;
		}
		
		SingleLinkedListNode<Integer> run1 = null;
		SingleLinkedListNode<Integer> run2 = head;
		while (run2 != null){
			SingleLinkedListNode<Integer> temp = run2.getNext();
			run2.setNext(run1);
			run1 = run2;
			run2 = temp;
		}
		
		return run2;
	}
}
