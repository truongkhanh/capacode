package code.linkedlist;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import code.classes.SingleLinkedListNode;

public class DuplicationRemove {
	
	/**
	 * Remove duplication in linkedlist by hash set
	 * @param head
	 * @return
	 */
	public static SingleLinkedListNode<Integer> removeDuplication1(SingleLinkedListNode<Integer> head){
		if(head == null || head.getNext() == null){
			return head;
		}
		
		Set<Integer> allValues = new HashSet<Integer>();
		allValues.add(head.getValue());
		
		//store the last added element
		SingleLinkedListNode<Integer> tail = head;
		
		//
		SingleLinkedListNode<Integer> run = head.getNext();
		while(run != null){
			//add new element
			if(!allValues.contains(run.getValue())){
				allValues.add(run.getValue());
				tail.setNext(run);
				
				tail = run;
			}
			
			run = run.getNext();
		}
		
		tail.setNext(null);
		
		return head;
	}
	
	/**
	 * Remove duplication in linkedlist by hash set
	 * @param head
	 * @return
	 */
	public static SingleLinkedListNode<Integer> removeDuplication2(SingleLinkedListNode<Integer> head){
		if(head == null || head.getNext() == null){
			return head;
		}
		
		Set<Integer> allValues = new HashSet<Integer>();
		allValues.add(head.getValue());
		
		//store the last added element
		SingleLinkedListNode<Integer> tail = head;
		
		//
		SingleLinkedListNode<Integer> run = head.getNext();
		while(run != null){
			//add new element
			if(!appearBefore(head, tail, run)){
				allValues.add(run.getValue());
				tail.setNext(run);
				
				tail = run;
			}
			
			run = run.getNext();
		}
		
		tail.setNext(null);
		
		return head;
	}
	
	private static boolean appearBefore(SingleLinkedListNode<Integer> head, SingleLinkedListNode<Integer> tail, SingleLinkedListNode<Integer> currentNode){
		while(true){
			if(head.getValue() == currentNode.getValue()){
				return true;
			}
			
			if(head == tail){
				break;
			}
			
			head = head.getNext();
		}
		
		return false;
	}
	
	public static SingleLinkedListNode<Integer> removeDuplication3(SingleLinkedListNode<Integer> head){
		if (head == null || head.getNext() == null){
			return head;
		}
		head = sort(head);
		
		//create a dummy node, remove at the end
		SingleLinkedListNode<Integer> result = new SingleLinkedListNode<Integer>(null);
		SingleLinkedListNode<Integer> lastAdded = result;
		SingleLinkedListNode<Integer> run = head;
		while (run != null){
			boolean isUnique = (run.getNext() == null || (run.getValue() != run.getNext().getValue())); 
			if (isUnique){
				lastAdded.setNext(run);
				lastAdded = run;
			}
			run = run.getNext();
		}
		
		return result.getNext();
	}
	private static SingleLinkedListNode<Integer> sort(SingleLinkedListNode<Integer> head)
	{
		if (head == null || head.getNext() == null){
			return head;
		}
		
		List<SingleLinkedListNode<Integer>> children = SplitLinkedList.split1(head);
		SingleLinkedListNode<Integer> left = sort(children.get(0));
		SingleLinkedListNode<Integer> right = sort(children.get(1));
		
		return merge(left, right);
	}
	
	private static SingleLinkedListNode<Integer> merge(SingleLinkedListNode<Integer> left, SingleLinkedListNode<Integer> right)
	{
		//create a dummy node, remove at the end
		SingleLinkedListNode<Integer> result = new SingleLinkedListNode<Integer>(null);
		SingleLinkedListNode<Integer> lastAdded = result;
		
		while (left != null && right != null){
			if (left.getValue() < right.getValue()){
				lastAdded.setNext(left);
				lastAdded = left;
				left = left.getNext();
			} else{
				lastAdded.setNext(right);
				lastAdded = right;
				right = right.getNext();
			}
		}
		
		if (left == null){
			lastAdded.setNext(right);
		} else{
			lastAdded.setNext(left);
		}
		
		return result.getNext();
	}
}
