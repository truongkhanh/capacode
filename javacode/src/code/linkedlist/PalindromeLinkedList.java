package code.linkedlist;

import java.util.Stack;

import code.classes.SingleLinkedListNode;

public class PalindromeLinkedList {
	public static boolean isPalindrome1(SingleLinkedListNode<Integer> head){
		Stack<Integer> values = new Stack<Integer>();
		SingleLinkedListNode<Integer> run = head;
		while(run != null){
			values.push(run.getValue());
			run = run.getNext();
		}
		
		run = head;
		while(run != null){
			int valueInStack = values.pop();
			if (valueInStack != run.getValue()){
				return false;
			}
			run = run.getNext();
		}
		
		return true;
	}
	
	public static boolean isPalindrome(SingleLinkedListNode<Integer> head){
		if (head == null){
			return true;
		}
		
		//run1 moves 1 step, run2 moves 2 steps
		//run2 reaches end, run1 is at middle
		SingleLinkedListNode<Integer> run1 = head;
		SingleLinkedListNode<Integer> run2 = head;
		int count = 0;
		while(run2 != null){
			run2 = run2.getNext();
			count++;
			if(run2 == null){
				break;
			}
			run2 = run2.getNext();
			count++;
			run1 = run1.getNext();
		}
		
		//run1 is now at middle of linked list
		//reverse half right part of linked list
		run2 = run1.getNext();
		while (run2 != null){
			SingleLinkedListNode<Integer> temp = run2.getNext();
			run2.setNext(run1);
			run1 = run2;
			run2 = temp;
		}
		
		//run1 is the last element of linked list
		SingleLinkedListNode<Integer> last = run1; //backup to reverse back later
		
		//check for palindrome
		run2 = head;
		boolean result = true;
		for (int i = 0; i < count / 2; i++){
			if (run1.getValue() != run2.getValue()){
				result = false;
				break;
			}
			run1 = run1.getNext();
			run2 = run2.getNext();
		}
		
		//reverse back
		run1 = last;
		run2 = run1.getNext();
		for (int i = 0; i < (count - 1) / 2; i++){
			SingleLinkedListNode<Integer> temp = run2.getNext();
			run2.setNext(run1);
			run1 = run2;
			run2 = temp;
		}
		
		return result;
	}
}
