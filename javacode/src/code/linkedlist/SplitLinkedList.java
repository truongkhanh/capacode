package code.linkedlist;

import java.util.ArrayList;
import java.util.List;

import code.classes.SingleLinkedListNode;

public class SplitLinkedList {
	public static List<SingleLinkedListNode<Integer>> split1(SingleLinkedListNode<Integer> head)
	{
		List<SingleLinkedListNode<Integer>> result = new ArrayList<SingleLinkedListNode<Integer>>();
		
		//empty or has one element, the second part is empty
		if (head == null || head.getNext() == null){
			result.add(head);
			result.add(null);
			return result;
		}
		
		//run1 moves 1 step, run2 moves 2 steps
		//run2 reaches end, run1 is at middle
		SingleLinkedListNode<Integer> run1 = head;
		SingleLinkedListNode<Integer> run2 = head;
		while(run2 != null){
			run2 = run2.getNext();
			if(run2 == null){
				break;
			}
			run2 = run2.getNext();
			run1 = run1.getNext();
		}
		
		//split
		SingleLinkedListNode<Integer> head2 = run1.getNext();
		run1.setNext(null);
		result.add(head);
		result.add(head2);
		return result;
	}
	
	public static List<SingleLinkedListNode<Integer>> split2(SingleLinkedListNode<Integer> head)
	{
		List<SingleLinkedListNode<Integer>> result = new ArrayList<SingleLinkedListNode<Integer>>();
		
		//empty or has one element, the second part is empty
		if (head == null || head.getNext() == null){
			result.add(head);
			result.add(null);
			return result;
		}
		
		int count = 0;
		SingleLinkedListNode<Integer> run = head;
		while (run != null){
			count++;
			run = run.getNext();
		}
		run = head;
		for(int i = 1; i < (count + 1) / 2; i++){
			run = run.getNext();
		}
		
		//split
		SingleLinkedListNode<Integer> head2 = run.getNext();
		run.setNext(null);
		result.add(head);
		result.add(head2);
		return result;
	}
}
