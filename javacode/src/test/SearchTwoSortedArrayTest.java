package test;

import static org.junit.Assert.*;

import org.junit.Test;

import code.array.SearchTwoSortedArrays;

public class SearchTwoSortedArrayTest {
	@Test
	public void test1(){
		int [] A = new int[0];
		int [] B = new int[]{3,5,10,15,27};
		
		int kElement = SearchTwoSortedArrays.findKthSmallest(1, A, B);
		assertEquals(3, kElement);
	}
	
	@Test
	public void test2(){
		int [] A = new int[0];
		int [] B = new int[]{3,5,10,15,27};
		
		int kElement = SearchTwoSortedArrays.findKthSmallest(2, A, B);
		assertEquals(5, kElement);
	}
	
	@Test
	public void test3(){
		int [] A = new int[0];
		int [] B = new int[]{3,5,10,15,27};
		
		int kElement = SearchTwoSortedArrays.findKthSmallest(3, A, B);
		assertEquals(10, kElement);
	}
	
	@Test
	public void test4(){
		int [] A = new int[0];
		int [] B = new int[]{3,5,10,15,27};
		
		int kElement = SearchTwoSortedArrays.findKthSmallest(5, A, B);
		assertEquals(27, kElement);
	}
	
	@Test
	public void test5(){
		int [] A = new int[0];
		int [] B = new int[]{3,5,10,15,27};
		
		int kElement = SearchTwoSortedArrays.findKthSmallest(1, B, A);
		assertEquals(3, kElement);
	}
	
	@Test
	public void test6(){
		int [] A = new int[0];
		int [] B = new int[]{3,5,10,15,27};
		
		int kElement = SearchTwoSortedArrays.findKthSmallest(2, B, A);
		assertEquals(5, kElement);
	}
	
	@Test
	public void test7(){
		int [] A = new int[0];
		int [] B = new int[]{3,5,10,15,27};
		
		int kElement = SearchTwoSortedArrays.findKthSmallest(3, B, A);
		assertEquals(10, kElement);
	}
	
	@Test
	public void test8(){
		int [] A = new int[0];
		int [] B = new int[]{3,5,10,15,27};
		
		int kElement = SearchTwoSortedArrays.findKthSmallest(5, B, A);
		assertEquals(27, kElement);
	}
	
	@Test
	public void test9(){
		int [] A = new int[]{2, 4, 8, 12};
		int [] B = new int[]{3,5,10,15,27};
		
		int kElement = SearchTwoSortedArrays.findKthSmallest(1, A, B);
		assertEquals(2, kElement);
	}
	
	@Test
	public void test10(){
		int [] A = new int[]{2, 4, 8, 12};
		int [] B = new int[]{3,5,10,15,27};
		
		int kElement = SearchTwoSortedArrays.findKthSmallest(4, A, B);
		assertEquals(5, kElement);
	}
	
	@Test
	public void test11(){
		int [] A = new int[]{2, 4, 8, 12};
		int [] B = new int[]{3,5,10,15,27};
		
		int kElement = SearchTwoSortedArrays.findKthSmallest(9, A, B);
		assertEquals(27, kElement);
	}
	
	@Test
	public void test12(){
		int [] A = new int[]{2, 4, 8, 12};
		int [] B = new int[]{3,5,10,15,27};
		
		int kElement = SearchTwoSortedArrays.findKthSmallest(7, A, B);
		assertEquals(12, kElement);
	}
	
	@Test
	public void test13(){
		int [] A = new int[]{2, 4, 8, 12};
		int [] B = new int[]{3,5,10,15,27};
		
		int kElement = SearchTwoSortedArrays.findKthSmallest(7, B, A);
		assertEquals(12, kElement);
	}
	
	@Test
	public void test14(){
		int [] A = new int[]{2, 4, 8, 12, 30, 40};
		int [] B = new int[]{3,5,10,15,27};
		
		int kElement = SearchTwoSortedArrays.findKthSmallest(9, A, B);
		assertEquals(27, kElement);
	}
	
	@Test
	public void test15(){
		int [] A = new int[]{2, 4, 8};
		int [] B = new int[]{10,15,27};
		
		int kElement = SearchTwoSortedArrays.findKthSmallest(3, A, B);
		assertEquals(8, kElement);
	}
	
	@Test
	public void test16(){
		int [] A = new int[]{5, 5, 5, 5};
		int [] B = new int[]{3, 5};
		
		int kElement = SearchTwoSortedArrays.findKthSmallest(3, A, B);
		assertEquals(5, kElement);
	}
}
