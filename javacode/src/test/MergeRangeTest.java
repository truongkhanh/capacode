package test;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import code.array.MergeRange;


public class MergeRangeTest {

	@Test
	public void whenAllTheSame(){
		MergeRange merger = new MergeRange();
		List<MergeRange.Range> input = Arrays.asList(merger.new Range(0, 1), merger.new Range(0, 1), merger.new Range(0, 1));
		List<MergeRange.Range> result = new MergeRange().merge(input);
		List<MergeRange.Range> expect = Arrays.asList(merger.new Range(0, 1));
		Assert.assertArrayEquals(expect.toArray(), result.toArray());		
	}
	
	@Test
	public void whenThereIsOverLap(){
		MergeRange merger = new MergeRange();
		List<MergeRange.Range> input = Arrays.asList(merger.new Range(0, 1), merger.new Range(3, 4), merger.new Range(1, 2));
		List<MergeRange.Range> result = new MergeRange().merge(input);
		List<MergeRange.Range> expect = Arrays.asList(merger.new Range(0, 2), merger.new Range(3, 4));
		Assert.assertArrayEquals(expect.toArray(), result.toArray());		
	}
	
	@Test
	public void whenAllOverLap(){
		MergeRange merger = new MergeRange();
		List<MergeRange.Range> input = Arrays.asList(merger.new Range(0, 2), merger.new Range(2, 4), merger.new Range(1, 3));
		List<MergeRange.Range> result = new MergeRange().merge(input);
		List<MergeRange.Range> expect = Arrays.asList(merger.new Range(0, 4));
		Assert.assertArrayEquals(expect.toArray(), result.toArray());		
	}
	
	@Test
	public void whenAllSeparate(){
		MergeRange merger = new MergeRange();
		List<MergeRange.Range> input = Arrays.asList(merger.new Range(0, 2), merger.new Range(30, 40), merger.new Range(3, 4));
		List<MergeRange.Range> result = new MergeRange().merge(input);
		List<MergeRange.Range> expect = Arrays.asList(merger.new Range(0, 2), merger.new Range(3, 4), merger.new Range(30, 40));
		Assert.assertArrayEquals(expect.toArray(), result.toArray());		
	}
	
	
	

}
