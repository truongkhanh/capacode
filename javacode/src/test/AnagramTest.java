package test;

import static org.junit.Assert.*;

import org.junit.Test;

import code.string.Anagram;

public class AnagramTest {

	@Test
	public void test1() {
		boolean isAnagram = Anagram.isAnagram("", "");
		assertTrue(isAnagram);
	}
	
	@Test
	public void test2() {
		boolean isAnagram = Anagram.isAnagram("", "abc");
		assertFalse(isAnagram);
	}
	
	@Test
	public void test3() {
		boolean isAnagram = Anagram.isAnagram("abca", "abc");
		assertFalse(isAnagram);
	}
	
	@Test
	public void test4() {
		boolean isAnagram = Anagram.isAnagram("abcab", "aabcb");
		assertTrue(isAnagram);
	}

}
