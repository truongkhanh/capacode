package test;

import static org.junit.Assert.*;

import org.junit.Test;

import code.array.PairWithSum;

public class PairWithSumTest {

	@Test
	public void test1() {
		int [] A = new int[]{ 8, 3, 2, 5, 1};
		int k = 7;
		
		boolean result = PairWithSum.havingPairWithSum(A, k);
		assertTrue(result);
	}
	
	@Test
	public void test2() {
		int [] A = new int[]{ 8, 3, 2, 5, 1};
		int k = 16;
		
		boolean result = PairWithSum.havingPairWithSum(A, k);
		assertFalse(result);
	}
	
	@Test
	public void test3() {
		int [] A = new int[]{ 8, 3, 2, 5, 1};
		int k = 12;
		
		boolean result = PairWithSum.havingPairWithSum(A, k);
		assertFalse(result);
	}
	
	@Test
	public void test4() {
		int [] A = new int[]{ 8, 3, 2, 5, 1};
		int k = 8;
		
		boolean result = PairWithSum.havingPairWithSum(A, k);
		assertTrue(result);
	}
	
	@Test
	public void test5() {
		int [] A = new int[]{ 5};
		int k = 10;
		
		boolean result = PairWithSum.havingPairWithSum(A, k);
		assertFalse(result);
	}

}
