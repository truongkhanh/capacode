package test;

import org.junit.Assert;
import org.junit.Test;

import code.array.ProductOfOtherNumber;

public class ProductOfOtherNumberTest {
	@Test
	public void testNonZeroElement()
	{
		int[] A = {2, 8, 7, 5};
		int[] result = {280, 70, 80, 112};
		int[] B = ProductOfOtherNumber.productOfOtherNumbers(A);
		Assert.assertArrayEquals(result, B);
		B = ProductOfOtherNumber.productOfOtherNumbersNoDivision(A);
		Assert.assertArrayEquals(result, B);
	}
	
	@Test
	public void testOneZeroElement()
	{
		int[] A = {2, 0, 7, 5};
		int[] result = {0, 70, 0, 0};
		int[] B = ProductOfOtherNumber.productOfOtherNumbers(A);
		Assert.assertArrayEquals(result, B);
		B = ProductOfOtherNumber.productOfOtherNumbersNoDivision(A);
		Assert.assertArrayEquals(result, B);
	}
	
	@Test
	public void testTwoZeroElements()
	{
		int[] A = {2, 0, 7, 0};
		int[] result = {0, 0, 0, 0};
		int[] B = ProductOfOtherNumber.productOfOtherNumbers(A);
		Assert.assertArrayEquals(result, B);
		B = ProductOfOtherNumber.productOfOtherNumbersNoDivision(A);
		Assert.assertArrayEquals(result, B);
	}
}
