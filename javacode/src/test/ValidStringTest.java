package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import code.string.ValidString;

public class ValidStringTest {
	@Test
	public void test1() {
		String[] words = new String[]{"mobile","samsung","sam","sung","man","mango",
                "icecream","and","go","i","like","ice","cream"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		boolean result = ValidString.isValidString(dictionary, "");
		assertTrue(result);
	}
	
	@Test
	public void test2() {
		String[] words = new String[]{"mobile","samsung","sam","sung","man","mango",
                "icecream","and","go","i","like","ice","cream"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		boolean result = ValidString.isValidString(dictionary, "ilikesamsung");
		assertTrue(result);
	}
	
	@Test
	public void test3() {
		String[] words = new String[]{"mobile","samsung","sam","sung","man","mango",
                "icecream","and","go","i","like","ice","cream"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		boolean result = ValidString.isValidString(dictionary, "iiiiiiii");
		assertTrue(result);
	}
	
	@Test
	public void test4() {
		String[] words = new String[]{"mobile","samsung","sam","sung","man","mango",
                "icecream","and","go","i","like","ice","cream"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		boolean result = ValidString.isValidString(dictionary, "ilikelikeimangoiii");
		assertTrue(result);
	}
	
	@Test
	public void test5() {
		String[] words = new String[]{"mobile","samsung","sam","sung","man","mango",
                "icecream","and","go","i","like","ice","cream"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		boolean result = ValidString.isValidString(dictionary, "samsungandmango");
		assertTrue(result);
	}
	
	@Test
	public void test6() {
		String[] words = new String[]{"mobile","samsung","sam","sung","man","mango",
                "icecream","and","go","i","like","ice","cream"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		boolean result = ValidString.isValidString(dictionary, "samsungandmangok");
		assertFalse(result);
	}
	
	@Test
	public void test7() {
		String[] words = new String[]{"I", "like", "coding", "is", "fun"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		boolean result = ValidString.isValidString(dictionary, "Ilikecoding");
		assertTrue(result);
	}
	
	@Test
	public void test8() {
		String[] words = new String[]{"I", "like", "coding", "is", "fun"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		boolean result = ValidString.isValidString(dictionary, "Ilikecode");
		assertFalse(result);
	}
	
	@Test
	public void test9() {
		String[] words = new String[]{"I", "like", "coding", "is", "fun"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		boolean result = ValidString.isValidString(dictionary, "codeisfun");
		assertFalse(result);
	}
	
	@Test
	public void test10() {
		String[] words = new String[]{"I", "like", "coding", "is", "fun"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		boolean result = ValidString.isValidString(dictionary, "Ilikefun");
		assertTrue(result);
	}
	
	@Test
	public void test11() {
		String[] words = new String[]{"I", "like", "coding", "is", "fun"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		boolean result = ValidString.isValidString(dictionary, "funiscode");
		assertFalse(result);
	}
}
