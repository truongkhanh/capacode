package test;


import org.junit.Assert;
import org.junit.Test;

import code.array.SortedArrayTransformation;


public class SortedArrayTransformationTest {

	
	@Test
	public void test2(){
		int[] A = new int[]{1};
		int numChanges = SortedArrayTransformation.computeNumChange(A);
		Assert.assertEquals(0, numChanges);
	}
	
	@Test
	public void test3(){
		int[] A = new int[]{1, 2, 3, 4, 5};
		int numChanges = SortedArrayTransformation.computeNumChange(A);
		Assert.assertEquals(0, numChanges);
	}
	
	@Test
	public void test4(){
		int[] A = new int[]{1, 1, 1, 1, 1};
		int numChanges = SortedArrayTransformation.computeNumChange(A);
		Assert.assertEquals(4, numChanges);
	}
	
	@Test
	public void test5(){
		int[] A = new int[]{0, 1, 2, 3, 3, 4};
		int numChanges = SortedArrayTransformation.computeNumChange(A);
		Assert.assertEquals(2, numChanges);
	}
	
	@Test
	public void test6(){
		int[] A = new int[]{1, 2, 3, 6, 5, 7};
		int numChanges = SortedArrayTransformation.computeNumChange(A);
		Assert.assertEquals(1, numChanges);
	}
	
	@Test
	public void test7(){
		int[] A = new int[]{7, 8, 3, 4, 7, 6};
		int numChanges = SortedArrayTransformation.computeNumChange(A);
		Assert.assertEquals(3, numChanges);
	}
}
