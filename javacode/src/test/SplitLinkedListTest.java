package test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import code.classes.SingleLinkedList;
import code.classes.SingleLinkedListNode;
import code.linkedlist.SplitLinkedList;

public class SplitLinkedListTest {
	enum Strategy {CountElements, TwoPointers};
	Strategy strategy = Strategy.CountElements;
	
	private List<SingleLinkedListNode<Integer>> split(SingleLinkedListNode<Integer> head){
		if (strategy == Strategy.TwoPointers){
			return SplitLinkedList.split1(head);
		}else{
			return SplitLinkedList.split2(head);
		}
	}
	
	@Test
	public void whenEmpty(){
		Integer[] elements = new Integer[]{};
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		
		List<SingleLinkedListNode<Integer>> subLists = split(head);
		
		Assert.assertEquals(subLists.get(0), null);
		Assert.assertEquals(subLists.get(1), null);
	}
	
	@Test
	public void whenHavingOneElement(){
		Integer[] elements = new Integer[]{1};
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		
		List<SingleLinkedListNode<Integer>> subLists = split(head);
		
		SingleLinkedList<Integer> subList1 = new SingleLinkedList<Integer>(subLists.get(0));
		Assert.assertTrue(subList1.equals(new Integer[]{1}));
		Assert.assertEquals(subLists.get(1), null);
	}
	
	@Test
	public void whenHavingTwoElements(){
		Integer[] elements = new Integer[]{1, 2};
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		
		List<SingleLinkedListNode<Integer>> subLists = split(head);
		
		SingleLinkedList<Integer> subList1 = new SingleLinkedList<Integer>(subLists.get(0));
		Assert.assertTrue(subList1.equals(new Integer[]{1}));
		
		SingleLinkedList<Integer> subList2 = new SingleLinkedList<Integer>(subLists.get(1));
		Assert.assertTrue(subList2.equals(new Integer[]{2}));
	}
	
	@Test
	public void whenHavingThreeElements(){
		Integer[] elements = new Integer[]{1, 2, 3};
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		
		List<SingleLinkedListNode<Integer>> subLists = split(head);
		
		SingleLinkedList<Integer> subList1 = new SingleLinkedList<Integer>(subLists.get(0));
		Assert.assertTrue(subList1.equals(new Integer[]{1, 2}));
		
		SingleLinkedList<Integer> subList2 = new SingleLinkedList<Integer>(subLists.get(1));
		Assert.assertTrue(subList2.equals(new Integer[]{3}));
	}
	
	@Test
	public void whenHavingFourElements(){
		Integer[] elements = new Integer[]{1, 2, 3, 4};
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		
		List<SingleLinkedListNode<Integer>> subLists = split(head);
		
		SingleLinkedList<Integer> subList1 = new SingleLinkedList<Integer>(subLists.get(0));
		Assert.assertTrue(subList1.equals(new Integer[]{1, 2}));
		
		SingleLinkedList<Integer> subList2 = new SingleLinkedList<Integer>(subLists.get(1));
		Assert.assertTrue(subList2.equals(new Integer[]{3, 4}));
	}

}
