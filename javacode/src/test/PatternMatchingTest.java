package test;

import static org.junit.Assert.*;

import org.junit.Test;

import code.string.PatternMatching;

public class PatternMatchingTest {

	@Test
	public void test1() {
		String pattern = "a";
		String S = "redgreenred";
		boolean isMatch = PatternMatching.isMatched(pattern, S);
		assertEquals(isMatch, true);
	}
	
	@Test
	public void test2() {
		String pattern = "ab";
		String S = "redgreenred";
		boolean isMatch = PatternMatching.isMatched(pattern, S);
		assertEquals(isMatch, true);
	}
	
	@Test
	public void test3() {
		String pattern = "aba";
		String S = "redgreenred";
		boolean isMatch = PatternMatching.isMatched(pattern, S);
		assertEquals(isMatch, true);
	}
	
	@Test
	public void test4() {
		String pattern = "abc";
		String S = "redgreenred";
		boolean isMatch = PatternMatching.isMatched(pattern, S);
		assertEquals(isMatch, true);
	}
	
	@Test
	public void test5() {
		String pattern = "aa";
		String S = "redgreenred";
		boolean isMatch = PatternMatching.isMatched(pattern, S);
		assertEquals(isMatch, false);
	}
	
	@Test
	public void test6() {
		String pattern = "aaa";
		String S = "redgreenred";
		boolean isMatch = PatternMatching.isMatched(pattern, S);
		assertEquals(isMatch, false);
	}
	
	@Test
	public void test7() {
		String pattern = "ab";
		String S = "aa";
		boolean isMatch = PatternMatching.isMatched(pattern, S);
		assertEquals(isMatch, false);
	}
	
	@Test
	public void test8() {
		String pattern = "ab";
		String S = "aaa";
		boolean isMatch = PatternMatching.isMatched(pattern, S);
		assertEquals(isMatch, true);
	}
	
	@Test
	public void test9() {
		String pattern = "abc";
		String S = "aaaaa";
		boolean isMatch = PatternMatching.isMatched(pattern, S);
		assertEquals(isMatch, false);
	}
	
	

}
