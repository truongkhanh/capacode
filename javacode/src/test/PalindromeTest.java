package test;

import static org.junit.Assert.*;

import org.junit.Test;

import code.string.Palindrome;

public class PalindromeTest {

	@Test
	public void test1() {
		String s = "";
		int longestPalindrome = Palindrome.longestPalindrome(s);
		assertEquals(longestPalindrome, 0);
	}
	
	@Test
	public void test2() {
		String s = "aa";
		int longestPalindrome = Palindrome.longestPalindrome(s);
		assertEquals(longestPalindrome, 2);
	}
	
	@Test
	public void test3() {
		String s = "aaaa";
		int longestPalindrome = Palindrome.longestPalindrome(s);
		assertEquals(longestPalindrome, 4);
	}
	
	@Test
	public void test4() {
		String s = "aaaaabbccc";
		int longestPalindrome = Palindrome.longestPalindrome(s);
		assertEquals(longestPalindrome,5);
	}
	
	@Test
	public void test5() {
		String s = "aaaabbccc";
		int longestPalindrome = Palindrome.longestPalindrome(s);
		assertEquals(longestPalindrome, 4);
	}
	
	@Test
	public void test6() {
		String s = "abcacba01221";
		int longestPalindrome = Palindrome.longestPalindrome(s);
		assertEquals(longestPalindrome, 7);
	}

}
