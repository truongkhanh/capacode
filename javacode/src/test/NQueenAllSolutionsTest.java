package test;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import code.recursion.NQueenAllSolutions;

public class NQueenAllSolutionsTest {

	private NQueenAllSolutions nQueenAllSolutions;
	
	@Before
	public void setup(){
		nQueenAllSolutions = new NQueenAllSolutions();
	}
	
	@Test
	public void whenSizeIs2ThenNoSolution(){
		List<int[]> result = nQueenAllSolutions.solve(2);
		
		Assert.assertEquals(0, result.size());
		
		for(int[] positions: result){
			Assert.assertTrue(nQueenAllSolutions.isASolution(positions));
		}
	}
	
	@Test
	public void whenSizeIs3ThenNoSolution(){
		List<int[]> result = nQueenAllSolutions.solve(3);
		
		Assert.assertEquals(0, result.size());
		
		for(int[] positions: result){
			Assert.assertTrue(nQueenAllSolutions.isASolution(positions));
		}
	}
	
	@Test
	public void whenSizeIs4ThenThereAre2Solutions(){
		List<int[]> result = nQueenAllSolutions.solve(4);
		
		Assert.assertEquals(2, result.size());
		
		for(int[] positions: result){
			Assert.assertTrue(nQueenAllSolutions.isASolution(positions));
		}
	}
	
	@Test
	public void whenSizeIs5ThenThereAre10Solutions(){
		List<int[]> result = nQueenAllSolutions.solve(5);
		
		Assert.assertEquals(10, result.size());
		
		for(int[] positions: result){
			Assert.assertTrue(nQueenAllSolutions.isASolution(positions));
		}
	}
	
	@Test
	public void whenSizeIs8ThenThereAre92Solutions(){
		List<int[]> result = nQueenAllSolutions.solve(8);
		
		Assert.assertEquals(92, result.size());
		
		for(int[] positions: result){
			Assert.assertTrue(nQueenAllSolutions.isASolution(positions));
		}
	}
	
	@Test
	public void whenSizeIs9ThenThereAre352Solutions(){
		List<int[]> result = nQueenAllSolutions.solve(9);
		
		Assert.assertEquals(352, result.size());
		
		for(int[] positions: result){
			Assert.assertTrue(nQueenAllSolutions.isASolution(positions));
		}
	}
	
	@Test
	public void whenSizeIs10ThenThereAre724Solutions(){
		List<int[]> result = nQueenAllSolutions.solve(10);
		
		Assert.assertEquals(724, result.size());
		
		for(int[] positions: result){
			Assert.assertTrue(nQueenAllSolutions.isASolution(positions));
		}
	}
	
	@Test
	public void whenSizeIs11ThenThereAre2680Solutions(){
		List<int[]> result = nQueenAllSolutions.solve(11);
		
		Assert.assertEquals(2680, result.size());
		
		for(int[] positions: result){
			Assert.assertTrue(nQueenAllSolutions.isASolution(positions));
		}
	}
}
