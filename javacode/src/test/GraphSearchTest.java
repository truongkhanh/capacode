package test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import code.graph.Edge;
import code.graph.Graph;
import code.graph.GraphSearch;

public class GraphSearchTest {

	@Test
	public void test1(){
		Graph graph = new Graph(4);
		graph.addEdge(new Edge(0, 0));
		graph.addEdge(new Edge(0, 1));
		graph.addEdge(new Edge(1, 2));
		graph.addEdge(new Edge(1, 3));
		graph.addEdge(new Edge(2, 2));
		
		Set<Integer> goal = new HashSet<Integer>();
		goal.add(0);
		Assert.assertTrue(GraphSearch.isReachableDFS(0, goal, graph));
		
		List<Edge> path = new ArrayList<Edge>();
		
		Assert.assertEquals(path, GraphSearch.DFS(0, goal, graph));
		
		//
		goal = new HashSet<Integer>();
		goal.add(1);
		Assert.assertTrue(GraphSearch.isReachableDFS(0, goal, graph));
		
		path = GraphSearch.DFS(0, goal, graph);
		Assert.assertTrue(path.size() >= 1);
		System.out.println(path);
		
		//
		goal = new HashSet<Integer>();
		goal.add(2);
		Assert.assertTrue(GraphSearch.isReachableDFS(0, goal, graph));
		
		path = GraphSearch.DFS(0, goal, graph);
		Assert.assertTrue(path.size() >=2);
		System.out.println(path);
		
		//
		goal = new HashSet<Integer>();
		goal.add(3);
		Assert.assertTrue(GraphSearch.isReachableDFS(0, goal, graph));
		
		path = GraphSearch.DFS(0, goal, graph);
		Assert.assertTrue(path.size() >=2);
		System.out.println(path);
		
		//
		for(int i = 1; i <= 3; i++){
			goal = new HashSet<Integer>();
			goal.add(0);
			Assert.assertFalse(GraphSearch.isReachableDFS(i, goal, graph));
		}
	}
	
	@Test
	public void test2(){
		Graph graph = new Graph(5);
		graph.addEdge(new Edge(0, 1));
		graph.addEdge(new Edge(1, 0));
		graph.addEdge(new Edge(1, 2));
		graph.addEdge(new Edge(2, 1));
		graph.addEdge(new Edge(2, 3));
		
		Set<Integer> goal = new HashSet<Integer>();
		goal.add(0);
		Assert.assertTrue(GraphSearch.isReachableDFS(0, goal, graph));
		
		List<Edge> path = new ArrayList<Edge>();
		
		Assert.assertEquals(path, GraphSearch.DFS(0, goal, graph));
		
		//
		goal = new HashSet<Integer>();
		goal.add(1);
		Assert.assertTrue(GraphSearch.isReachableDFS(0, goal, graph));
		
		path = GraphSearch.DFS(0, goal, graph);
		Assert.assertTrue(path.size() >= 1);
		System.out.println(path);
		
		//
		goal = new HashSet<Integer>();
		goal.add(2);
		Assert.assertTrue(GraphSearch.isReachableDFS(0, goal, graph));
		
		path = GraphSearch.DFS(0, goal, graph);
		Assert.assertTrue(path.size() >= 2);
		System.out.println(path);
		
		//
		goal = new HashSet<Integer>();
		goal.add(3);
		Assert.assertTrue(GraphSearch.isReachableDFS(0, goal, graph));
		
		path = GraphSearch.DFS(0, goal, graph);
		Assert.assertTrue(path.size() >= 3);
		System.out.println(path);
		
		//
		goal = new HashSet<Integer>();
		goal.add(4);
		Assert.assertFalse(GraphSearch.isReachableDFS(0, goal, graph));

	}
}
