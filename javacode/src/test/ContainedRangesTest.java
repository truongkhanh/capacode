package test;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import code.array.MergeRange;

public class ContainedRangesTest {

	@Test
	public void whenHavingNoRange(){
		MergeRange merger = new MergeRange();
		List<MergeRange.Range> input = Arrays.asList();
		boolean expected = false;
		boolean result = merger.hasContainedRange(input);
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenOneRange(){
		MergeRange merger = new MergeRange();
		List<MergeRange.Range> input = Arrays.asList(merger.new Range(0, 1));
		boolean expected = false;
		boolean result = merger.hasContainedRange(input);
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenHavingNoContainedRange(){
		MergeRange merger = new MergeRange();
		List<MergeRange.Range> input = Arrays.asList(merger.new Range(1, 3), merger.new Range(5, 9), merger.new Range(2, 4));
		boolean expected = false;
		boolean result = merger.hasContainedRange(input);
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenHavingContainedRange(){
		MergeRange merger = new MergeRange();
		List<MergeRange.Range> input = Arrays.asList(merger.new Range(1, 3), merger.new Range(5, 9), merger.new Range(2, 4), merger.new Range(6, 10), merger.new Range(7, 8));
		boolean expected = true;
		boolean result = merger.hasContainedRange(input);
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenAllRangesAreSame(){
		MergeRange merger = new MergeRange();
		List<MergeRange.Range> input = Arrays.asList(merger.new Range(1, 3), merger.new Range(1, 3), merger.new Range(1, 3));
		boolean expected = true;
		boolean result = merger.hasContainedRange(input);
		Assert.assertEquals(expected, result);
	}
}
