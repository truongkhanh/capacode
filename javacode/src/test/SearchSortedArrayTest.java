package test;

import org.junit.Test;
import static org.junit.Assert.*;
import code.array.SearchSortedArray;

public class SearchSortedArrayTest {

	@Test
	public void test1(){
		int [] A = new int[]{0, 0, 0, 0, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5};
		int K = 0;
		
		int index = SearchSortedArray.search(A, K);
		boolean cond = (index >= 0) && (index <= 3);
		assertTrue(cond);
		
		index = SearchSortedArray.searchSmallestIndex(A, K);
		assertEquals(index, 0);
		
		index = SearchSortedArray.searchLargestIndex(A, K);
		assertEquals(index, 3);
		
		index = SearchSortedArray.searchSmallestLarger(A, K);
		assertEquals(index, 4);
		
		index = SearchSortedArray.searchLargestSmaller(A, K);
		assertEquals(index, -1);
	}
	
	@Test
	public void test2(){
		int [] A = new int[]{0, 0, 0, 0, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5};
		int K = 1;
		
		int index = SearchSortedArray.search(A, K);
		boolean cond = (index >= 4) && (index <= 4);
		assertTrue(cond);
		
		index = SearchSortedArray.searchSmallestIndex(A, K);
		assertEquals(index, 4);
		
		index = SearchSortedArray.searchLargestIndex(A, K);
		assertEquals(index, 4);
		
		index = SearchSortedArray.searchSmallestLarger(A, K);
		assertEquals(index, 5);
		
		index = SearchSortedArray.searchLargestSmaller(A, K);
		assertEquals(index, 3);
	}
	
	@Test
	public void test3(){
		int [] A = new int[]{0, 0, 0, 0, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5};
		int K = 2;
		
		int index = SearchSortedArray.search(A, K);
		boolean cond = (index >= 5) && (index <= 8);
		assertTrue(cond);
		
		index = SearchSortedArray.searchSmallestIndex(A, K);
		assertEquals(index, 5);
		
		index = SearchSortedArray.searchLargestIndex(A, K);
		assertEquals(index, 8);
		
		index = SearchSortedArray.searchSmallestLarger(A, K);
		assertEquals(index, 9);
		
		index = SearchSortedArray.searchLargestSmaller(A, K);
		assertEquals(index, 4);
	}
	
	@Test
	public void test4(){
		int [] A = new int[]{0, 0, 0, 0, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5};
		int K = 3;
		
		int index = SearchSortedArray.search(A, K);
		boolean cond = (index >= 9) && (index <= 11);
		assertTrue(cond);
		
		index = SearchSortedArray.searchSmallestIndex(A, K);
		assertEquals(index, 9);
		
		index = SearchSortedArray.searchLargestIndex(A, K);
		assertEquals(index, 11);
		
		index = SearchSortedArray.searchSmallestLarger(A, K);
		assertEquals(index, 12);
		
		index = SearchSortedArray.searchLargestSmaller(A, K);
		assertEquals(index, 8);
	}
	
	@Test
	public void test5(){
		int [] A = new int[]{0, 0, 0, 0, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5};
		int K = 4;
		
		int index = SearchSortedArray.search(A, K);
		boolean cond = (index >= 12) && (index <= 13);
		assertTrue(cond);
		
		index = SearchSortedArray.searchSmallestIndex(A, K);
		assertEquals(index, 12);
		
		index = SearchSortedArray.searchLargestIndex(A, K);
		assertEquals(index, 13);
		
		index = SearchSortedArray.searchSmallestLarger(A, K);
		assertEquals(index, 14);
		
		index = SearchSortedArray.searchLargestSmaller(A, K);
		assertEquals(index, 11);
	}
	
	@Test
	public void test6(){
		int [] A = new int[]{0, 0, 0, 0, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5};
		int K = 5;
		
		int index = SearchSortedArray.search(A, K);
		boolean cond = (index >= 14) && (index <= 14);
		assertTrue(cond);
		
		index = SearchSortedArray.searchSmallestIndex(A, K);
		assertEquals(index, 14);
		
		index = SearchSortedArray.searchLargestIndex(A, K);
		assertEquals(index, 14);
		
		index = SearchSortedArray.searchSmallestLarger(A, K);
		assertEquals(index, 15);
		
		index = SearchSortedArray.searchLargestSmaller(A, K);
		assertEquals(index, 13);
	}
	
	@Test
	public void test7(){
		int [] A = new int[]{0, 0, 0, 0, 1, 3, 3, 3, 4, 4, 5};
		int K = 2;
		
		int index = SearchSortedArray.search(A, K);
		assertEquals(index, -1);
		
		index = SearchSortedArray.searchSmallestIndex(A, K);
		assertEquals(index, -1);
		
		index = SearchSortedArray.searchLargestIndex(A, K);
		assertEquals(index, -1);
		
		index = SearchSortedArray.searchSmallestLarger(A, K);
		assertEquals(index, 5);
		
		index = SearchSortedArray.searchLargestSmaller(A, K);
		assertEquals(index, 4);
	}
	
	@Test
	public void test8(){
		int [] A = new int[]{0, 0, 0, 0, 1, 3, 3, 3, 4, 4, 5};
		int K = -1;
		
		int index = SearchSortedArray.search(A, K);
		assertEquals(index, -1);
		
		index = SearchSortedArray.searchSmallestIndex(A, K);
		assertEquals(index, -1);
		
		index = SearchSortedArray.searchLargestIndex(A, K);
		assertEquals(index, -1);
		
		index = SearchSortedArray.searchSmallestLarger(A, K);
		assertEquals(index, 0);
		
		index = SearchSortedArray.searchLargestSmaller(A, K);
		assertEquals(index, -1);
	}
	
	@Test
	public void test9(){
		int [] A = new int[]{0, 0, 0, 0, 1, 3, 3, 3, 4, 4, 5};
		int K = 6;
		
		int index = SearchSortedArray.search(A, K);
		assertEquals(index, -1);
		
		index = SearchSortedArray.searchSmallestIndex(A, K);
		assertEquals(index, -1);
		
		index = SearchSortedArray.searchLargestIndex(A, K);
		assertEquals(index, -1);
		
		index = SearchSortedArray.searchSmallestLarger(A, K);
		assertEquals(index, A.length);
		
		index = SearchSortedArray.searchLargestSmaller(A, K);
		assertEquals(index, A.length - 1);
	}
	
	@Test
	public void test10(){
		int [] A = new int[]{5};
		int K = 5;
		
		int index = SearchSortedArray.search(A, K);
		assertEquals(index, 0);
		
		index = SearchSortedArray.searchSmallestIndex(A, K);
		assertEquals(index, 0);
		
		index = SearchSortedArray.searchLargestIndex(A, K);
		assertEquals(index, 0);
		
		index = SearchSortedArray.searchSmallestLarger(A, K);
		assertEquals(index, A.length);
		
		index = SearchSortedArray.searchLargestSmaller(A, K);
		assertEquals(index, - 1);
	}
	
	@Test
	public void test11(){
		int [] A = new int[]{};
		int K = 4;
		
		int index = SearchSortedArray.search(A, K);
		assertEquals(index, -1);
		
		index = SearchSortedArray.searchSmallestIndex(A, K);
		assertEquals(index, -1);
		
		index = SearchSortedArray.searchLargestIndex(A, K);
		assertEquals(index, -1);
		
		index = SearchSortedArray.searchSmallestLarger(A, K);
		assertEquals(index, A.length);
		
		index = SearchSortedArray.searchLargestSmaller(A, K);
		assertEquals(index, - 1);
	}
}
