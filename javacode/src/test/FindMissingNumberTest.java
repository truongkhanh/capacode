package test;

import org.junit.Assert;
import org.junit.Test;

import code.array.FindMissingNumber;

public class FindMissingNumberTest {

	@Test
	public void whenArrayIsEmpty(){
		int A[] = new int[0];
		int expect = 0;
		int result = FindMissingNumber.find2(A);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenArrayHasOneElementMissingOne(){
		int A[] = new int[]{0};
		int expect = 1;
		int result = FindMissingNumber.find2(A);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenArrayHasOneElementMissingZero(){
		int A[] = new int[]{1};
		int expect = 0;
		int result = FindMissingNumber.find2(A);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenArrayFiveElements(){
		int A[] = new int[]{1, 2, 4, 5, 0};
		int expect = 3;
		int result = FindMissingNumber.find2(A);
		Assert.assertEquals(expect, result);
	}
}
