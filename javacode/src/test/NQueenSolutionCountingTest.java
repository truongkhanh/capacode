package test;

import org.junit.Assert;
import org.junit.Test;

import code.recursion.NQueenSolutionCounting;

public class NQueenSolutionCountingTest {

	@Test
	public void whenSizeIs2ThenNoSolution(){
		int result = new NQueenSolutionCounting().solve(2);
		
		Assert.assertEquals(0, result);
	}
	
	@Test
	public void whenSizeIs3ThenNoSolution(){
		int result = new NQueenSolutionCounting().solve(3);
		
		Assert.assertEquals(0, result);
	}
	
	@Test
	public void whenSizeIs4ThenThereAre2Solutions(){
		int result = new NQueenSolutionCounting().solve(4);
		
		Assert.assertEquals(2, result);
	}
	
	@Test
	public void whenSizeIs5ThenThereAre10Solutions(){
		int result = new NQueenSolutionCounting().solve(5);
		
		Assert.assertEquals(10, result);
	}
	
	@Test
	public void whenSizeIs6ThenThereAre4Solutions(){
		int result = new NQueenSolutionCounting().solve(6);
		
		Assert.assertEquals(4, result);
	}
	
	@Test
	public void whenSizeIs7ThenThereAre40Solutions(){
		int result = new NQueenSolutionCounting().solve(7);
		
		Assert.assertEquals(40, result);
	}
	
	@Test
	public void whenSizeIs8ThenThereAre92Solutions(){
		int result = new NQueenSolutionCounting().solve(8);
		
		Assert.assertEquals(92, result);
	}
	
	@Test
	public void whenSizeIs9ThenThereAre352Solutions(){
		int result = new NQueenSolutionCounting().solve(9);
		
		Assert.assertEquals(352, result);
	}
	
	@Test
	public void whenSizeIs10ThenThereAre724Solutions(){
		int result = new NQueenSolutionCounting().solve(10);
		
		Assert.assertEquals(724, result);
	}
	
	@Test
	public void whenSizeIs11ThenThereAre2680Solutions(){
		int result = new NQueenSolutionCounting().solve(11);
		
		Assert.assertEquals(2680, result);
	}
	
	@Test
	public void whenSizeIs14ThenThereAre365596Solutions(){
		int result = new NQueenSolutionCounting().solve(14);
		
		Assert.assertEquals(365596, result);
	}
	
	@Test
	public void whenSizeIs15ThenThereAre2279184Solutions(){
		int result = new NQueenSolutionCounting().solve(15);
		
		Assert.assertEquals(2279184, result);
	}
}
