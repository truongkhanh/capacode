package test;

import org.junit.Assert;
import org.junit.Test;

import code.dynamicprogramming.LongestUniqueCharSubString;

public class LongestUniqueCharSubStringTest {

	@Test
	public void whenStringEmpty(){
		String input = "";
		String expect = "";
		String result = LongestUniqueCharSubString.find(input);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenStringHasOneChar(){
		String input = "a";
		String expect = "a";
		String result = LongestUniqueCharSubString.find(input);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenStringHasUniqueChar(){
		String input = "abcd";
		String expect = "abcd";
		String result = LongestUniqueCharSubString.find(input);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenStringHasSameChar(){
		String input = "aaaa";
		String expect = "a";
		String result = LongestUniqueCharSubString.find(input);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenLongestAtMostLeft(){
		String input = "abca";
		String expect = "abc";
		String result = LongestUniqueCharSubString.find(input);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenLongestAtMostRight(){
		String input = "ababc";
		String expect = "abc";
		String result = LongestUniqueCharSubString.find(input);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenLongestInMiddle(){
		String input = "ababcba";
		String expect = "abc";
		String result = LongestUniqueCharSubString.find(input);
		Assert.assertEquals(expect, result);
	}
}
