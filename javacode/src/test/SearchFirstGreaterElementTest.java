package test;

import org.junit.Assert;
import org.junit.Test;

import code.array.SearchFirstGreaterElement;

public class SearchFirstGreaterElementTest {
	@Test
	public void whenExists() {
		int [] A = new int[]{-1, 0, 1, 3, 6, 8};
		
		int k = 2;
		int expected = 3;
		int result = SearchFirstGreaterElement.search(A, k);
		
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenNotExists() {
		int [] A = new int[]{-1, 0, 1, 3, 6, 8};
		
		int k = 10;
		int expected = -1;
		int result = SearchFirstGreaterElement.search(A, k);
		
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenResultIsFirstElement() {
		int [] A = new int[]{-1, 0, 1, 3, 6, 8};
		
		int k = -2;
		int expected = 0;
		int result = SearchFirstGreaterElement.search(A, k);
		
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenResultIsLastElement() {
		int [] A = new int[]{-1, 0, 1, 3, 6, 8};
		
		int k = 7;
		int expected = 5;
		int result = SearchFirstGreaterElement.search(A, k);
		
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenThereAreDuplicate() {
		int [] A = new int[]{-1, 0, 1, 3, 3, 3, 6, 8};
		
		int k = 2;
		int expected = 3;
		int result = SearchFirstGreaterElement.search(A, k);
		
		Assert.assertEquals(expected, result);
	}
}
