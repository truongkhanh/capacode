package test;

import static org.junit.Assert.*;

import org.junit.Test;

import code.array.LongestIncreasingSubsequence;

public class LongestIncreasingSubsequenceTest {

	/**
	 * Check actual have the same length with expected
	 * Check actual is the increasing subsequence
	 * @param expected
	 * @param actual
	 * @param A
	 */
	private void check(int[] expected, int[] actual, int[] A){
		assertEquals(expected.length, actual.length);
		int count = 0;
		for(int i = 0; count < expected.length && i < A.length; i++){
			if(A[i] == actual[count]){
				count++;
			}
		}
		assertEquals(expected.length, count);
		
		for(int i = 1 ; i < actual.length; i++){
			assertTrue(actual[i] > actual[i-1]);
		}
	}
	
	@Test
	public void test1() {
		int[] A = new int[]{0, 8, 6, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15};
		int[] expected = new int[]{0, 2, 6, 9, 13, 15};
		int[] actual = LongestIncreasingSubsequence.longestIncreasingSubsequence1(A);
		check(expected, actual, A);
		
		actual = LongestIncreasingSubsequence.longestIncreasingSubsequence2(A);
		check(expected, actual, A);
	}
	
	@Test
	public void test2() {
		int[] A = new int[]{0, 8, 1, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15};
		int[] expected = new int[]{0, 1, 2, 6, 9, 13, 15};
		int[] actual = LongestIncreasingSubsequence.longestIncreasingSubsequence1(A);
		check(expected, actual, A);
		
		actual = LongestIncreasingSubsequence.longestIncreasingSubsequence2(A);
		check(expected, actual, A);
	}
	
	@Test
	public void test3() {
		int[] A = new int[]{10, 22, 9, 33, 21, 50, 49, 60, 80 };
		int[] expected = new int[]{10, 22, 33, 50, 60, 80};
		int[] actual = LongestIncreasingSubsequence.longestIncreasingSubsequence1(A);
		check(expected, actual, A);
		
		actual = LongestIncreasingSubsequence.longestIncreasingSubsequence2(A);
		check(expected, actual, A);
	}
	
	@Test
	public void test4() {
		int[] A = new int[]{3,2,6,4,5,1 };
		int[] expected = new int[]{3, 4, 5};
		int[] actual = LongestIncreasingSubsequence.longestIncreasingSubsequence1(A);
		check(expected, actual, A);
		
		actual = LongestIncreasingSubsequence.longestIncreasingSubsequence2(A);
		check(expected, actual, A);
	}
	
	@Test
	public void test5() {
		int[] A = new int[]{9,8,7,6 };
		int[] expected = new int[]{9};
		int[] actual = LongestIncreasingSubsequence.longestIncreasingSubsequence1(A);
		check(expected, actual, A);
		
		actual = LongestIncreasingSubsequence.longestIncreasingSubsequence2(A);
		check(expected, actual, A);

	}
	
	@Test
	public void test6() {
		int[] A = new int[]{2, 2, 2, 2 };
		int[] expected = new int[]{2};
		int[] actual = LongestIncreasingSubsequence.longestIncreasingSubsequence1(A);
		assertArrayEquals(expected, actual);
		
		actual = LongestIncreasingSubsequence.longestIncreasingSubsequence2(A);
		assertArrayEquals(expected, actual);

	}
	
	@Test
	public void test7() {
		int[] A = new int[]{6, 7, 8, 9 };
		int[] expected = A;
		int[] actual = LongestIncreasingSubsequence.longestIncreasingSubsequence1(A);
		assertArrayEquals(expected, actual);
		
		actual = LongestIncreasingSubsequence.longestIncreasingSubsequence2(A);
		assertArrayEquals(expected, actual);

	}
	
	@Test
	public void test8() {
		int[] A = new int[]{1, 2, 3, 0, 5, 7, 4 };
		int[] expected = new int[]{1, 2, 3, 5, 7};
		int[] actual = LongestIncreasingSubsequence.longestIncreasingSubsequence1(A);
		assertArrayEquals(expected, actual);
		
		actual = LongestIncreasingSubsequence.longestIncreasingSubsequence2(A);
		assertArrayEquals(expected, actual);

	}

}
