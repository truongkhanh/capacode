package test;

import org.junit.Assert;
import org.junit.Test;

import code.classes.SingleLinkedList;
import code.classes.SingleLinkedListNode;
import code.linkedlist.PalindromeLinkedList;

public class PalindromeLinkedListTest {
	@Test
	public void whenEmpty(){
		Integer[] elements = new Integer[]{};
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		boolean expect = true;
		boolean result = PalindromeLinkedList.isPalindrome(head);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenHasOneElement(){
		Integer[] elements = new Integer[]{1};
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		boolean expect = true;
		boolean result = PalindromeLinkedList.isPalindrome(head);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenHasTwoElementsButNotPalindrome(){
		Integer[] elements = new Integer[]{1, 2};
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		boolean expect = false;
		boolean result = PalindromeLinkedList.isPalindrome(head);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenHasTwoElements(){
		Integer[] elements = new Integer[]{1, 1};
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		boolean expect = true;
		boolean result = PalindromeLinkedList.isPalindrome(head);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenHasThreeElementsButNotPalindrome(){
		Integer[] elements = new Integer[]{1, 2, 3};
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		boolean expect = false;
		boolean result = PalindromeLinkedList.isPalindrome(head);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenHasThreeElements(){
		Integer[] elements = new Integer[]{1, 2, 1};
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		boolean expect = true;
		boolean result = PalindromeLinkedList.isPalindrome(head);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenHasSixElements(){
		Integer[] elements = new Integer[]{1, 2, 3, 3, 2, 1};
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		boolean expect = true;
		boolean result = PalindromeLinkedList.isPalindrome(head);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenHasSixElementsButNotPalindrome(){
		Integer[] elements = new Integer[]{1, 2, 3, 3, 2, 2};
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		boolean expect = false;
		boolean result = PalindromeLinkedList.isPalindrome(head);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenHasSevenElements(){
		Integer[] elements = new Integer[]{1, 2, 3, 4, 3, 2, 1};
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		boolean expect = true;
		boolean result = PalindromeLinkedList.isPalindrome(head);
		Assert.assertEquals(expect, result);
	}
	
	@Test
	public void whenHasSevenElementsButNotPalindrome(){
		Integer[] elements = new Integer[]{1, 2, 3, 4, 3, 3, 1};
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		boolean expect = false;
		boolean result = PalindromeLinkedList.isPalindrome(head);
		Assert.assertEquals(expect, result);
	}
}
