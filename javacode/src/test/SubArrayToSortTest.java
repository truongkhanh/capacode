package test;

import org.junit.Assert;
import org.junit.Test;

import code.array.SubArrayToSort;

public class SubArrayToSortTest {

	@Test
	public void test1(){
		int[] A = new int[]{1};
		int[] expected = new int[]{-1, -1};
		
		int[] result = SubArrayToSort.search(A);
		
		Assert.assertArrayEquals(expected, result);
	}
	
	@Test
	public void test2(){
		int[] A = new int[]{1, 2, 3, 4, 5};
		int[] expected = new int[]{-1, -1};
		
		int[] result = SubArrayToSort.search(A);
		
		Assert.assertArrayEquals(expected, result);
	}
	
	@Test
	public void test3(){
		int[] A = new int[]{1, 2, 3, 3, 3, 4, 4, 5};
		int[] expected = new int[]{-1, -1};
		
		int[] result = SubArrayToSort.search(A);
		
		Assert.assertArrayEquals(expected, result);
	}
	
	@Test
	public void test4(){
		int[] A = new int[]{1, 1, 1, 1, 1};
		int[] expected = new int[]{-1, -1};
		
		int[] result = SubArrayToSort.search(A);
		
		Assert.assertArrayEquals(expected, result);
	}
	
	@Test
	public void test5(){
		int[] A = new int[]{5, 4, 3, 2, 2, 1};
		int[] expected = new int[]{0, A.length -1};
		
		int[] result = SubArrayToSort.search(A);
		
		Assert.assertArrayEquals(expected, result);
	}
	
	@Test
	public void test6(){
		int[] A = new int[]{1, 2, 3, 7, 6, 4, 5, 8, 9, 10};
		int[] expected = new int[]{3, 6};
		
		int[] result = SubArrayToSort.search(A);
		
		Assert.assertArrayEquals(expected, result);
	}
	
	@Test
	public void test7(){
		int[] A = new int[]{1, 2, 3, 7, 7, 6, 6, 8, 9, 10};
		int[] expected = new int[]{3, 6};
		
		int[] result = SubArrayToSort.search(A);
		
		Assert.assertArrayEquals(expected, result);
	}
}
