package test;

import static org.junit.Assert.*;

import org.junit.Test;

import code.string.FirstUniqueChar;

public class FirstUniqueCharTest {

	@Test
	public void test1() {
		String input = "";
		int output = FirstUniqueChar.firstUniqueChar(input);
		assertEquals(output, -1);
	}
	
	@Test
	public void test2() {
		String input = "aaa";
		int output = FirstUniqueChar.firstUniqueChar(input);
		assertEquals(output, -1);
	}
	
	@Test
	public void test3() {
		String input = "aabcdb";
		int output = FirstUniqueChar.firstUniqueChar(input);
		assertEquals(output, 3);
	}
	
	@Test
	public void test4() {
		String input = "aabb";
		int output = FirstUniqueChar.firstUniqueChar(input);
		assertEquals(output, -1);
	}
	
	@Test
	public void test5() {
		String input = "aa bb";
		int output = FirstUniqueChar.firstUniqueChar(input);
		assertEquals(output, 2);
	}
}
