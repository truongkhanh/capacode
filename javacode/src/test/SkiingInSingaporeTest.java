package test;

import java.io.FileNotFoundException;

import org.junit.Assert;
import org.junit.Test;

import code.graph.SkiingInSingapore;

public class SkiingInSingaporeTest {
	@Test
	public void runSample() throws FileNotFoundException{
		SkiingInSingapore solver = new SkiingInSingapore();
		String output = solver.longestSteepestPath("src/input/sample.txt");
		String expected = "58";
		Assert.assertEquals(expected, output);
	}
	
	@Test
	public void runRealExample() throws FileNotFoundException{
		SkiingInSingapore solver = new SkiingInSingapore();
		String output = solver.longestSteepestPath("src/input/map.txt");
		String expected = "151422";
		Assert.assertEquals(expected, output);
	}
}
