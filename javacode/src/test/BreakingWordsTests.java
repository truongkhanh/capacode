package test;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import code.string.BreakingWords;

public class BreakingWordsTests {

	@Test
	public void test1() {
		String[] words = new String[]{"mobile","samsung","sam","sung","man","mango",
                "icecream","and","go","i","like","ice","cream"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		String result = BreakingWords.breakingWords(dictionary, "");
		assertTrue(result.equals("") );
	}
	
	@Test
	public void test2() {
		String[] words = new String[]{"mobile","samsung","sam","sung","man","mango",
                "icecream","and","go","i","like","ice","cream"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		String result = BreakingWords.breakingWords(dictionary, "ilikesamsung");
		assertTrue(result.equals("i like sam sung"));
	}
	
	@Test
	public void test3() {
		String[] words = new String[]{"mobile","samsung","sam","sung","man","mango",
                "icecream","and","go","i","like","ice","cream"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		String result = BreakingWords.breakingWords(dictionary, "iiiiiiii");
		assertTrue(result.equals("i i i i i i i i"));
	}
	
	@Test
	public void test4() {
		String[] words = new String[]{"mobile","samsung","sam","sung","man","mango",
                "icecream","and","go","i","like","ice","cream"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		String result = BreakingWords.breakingWords(dictionary, "ilikelikeimangoiii");
		assertTrue(result.equals("i like like i man go i i i"));
	}
	
	@Test
	public void test5() {
		String[] words = new String[]{"mobile","samsung","sam","sung","man","mango",
                "icecream","and","go","i","like","ice","cream"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		String result = BreakingWords.breakingWords(dictionary, "samsungandmango");
		assertTrue(result.equals("sam sung and man go"));
	}
	
	@Test
	public void test6() {
		String[] words = new String[]{"mobile","samsung","sam","sung","man","mango",
                "icecream","and","go","i","like","ice","cream"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		String result = BreakingWords.breakingWords(dictionary, "samsungandmangok");
		assertTrue(result.equals(""));
	}
	
	@Test
	public void test7() {
		String[] words = new String[]{"I", "like", "coding", "is", "fun"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		String result = BreakingWords.breakingWords(dictionary, "Ilikecoding");
		assertTrue(result.equals("I like coding"));
	}
	
	@Test
	public void test8() {
		String[] words = new String[]{"I", "like", "coding", "is", "fun"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		String result = BreakingWords.breakingWords(dictionary, "Ilikefun");
		assertTrue(result.equals("I like fun"));
	}
	
	@Test
	public void test9() {
		String[] words = new String[]{"I", "like", "coding", "is", "fun"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		String result = BreakingWords.breakingWords(dictionary, "codingisfun");
		assertTrue(result.equals("coding is fun"));
	}
	
	@Test
	public void test10() {
		String[] words = new String[]{"I", "like", "coding", "is", "fun"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		String result = BreakingWords.breakingWords(dictionary, "Ilikecode");
		assertTrue(result.equals(""));
	}
	
	@Test
	public void test11() {
		String[] words = new String[]{"I", "like", "coding", "is", "fun"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		String result = BreakingWords.breakingWords(dictionary, "codingisnotfun");
		assertTrue(result.equals(""));
	}
	
	@Test
	public void test12() {
		String[] words = new String[]{"I", "like", "coding", "is", "fun"};
		
		Set<String> dictionary = new HashSet<>(Arrays.asList(words));
		
		String result = BreakingWords.breakingWords(dictionary, "funiscode");
		assertTrue(result.equals(""));
	}

}
