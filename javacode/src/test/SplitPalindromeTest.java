package test;

import org.junit.Assert;
import org.junit.Test;

import code.string.SplitPalindrome;

public class SplitPalindromeTest {
	
	@Test
	public void whenHavingOnePalindrome()
	{
		String s = "aaaa";
		int expected = 0;
		int actual = SplitPalindrome.split(s);
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void whenHavingTwoPalindromes()
	{
		String s = "abbab";
		int expected = 1;
		int actual = SplitPalindrome.split(s);
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void whenHavingThreePalindromes()
	{
		String s = "dabac";
		int expected = 2;
		int actual = SplitPalindrome.split(s);
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void whenLongestPalindromeNotOptimal()
	{
		String s = "aaaaaadda";
		int expected = 1;
		int actual = SplitPalindrome.split(s);
		Assert.assertEquals(expected, actual);
	}
}
