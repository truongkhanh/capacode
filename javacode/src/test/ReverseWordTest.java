package test;

import static org.junit.Assert.*;

import org.junit.Test;

import code.string.ReverseWord;

public class ReverseWordTest {

	@Test
	public void test1() {
		String input = "";
		String output = ReverseWord.reverseWord(input);
		assertEquals(output, "");
	}
	
	@Test
	public void test2() {
		String input = "   ";
		String output = ReverseWord.reverseWord(input);
		assertEquals(output, "   ");
	}
	
	@Test
	public void test3() {
		String input = "aaa";
		String output = ReverseWord.reverseWord(input);
		assertEquals(output, "aaa");
	}
	
	@Test
	public void test4() {
		String input = "abc ab a";
		String output = ReverseWord.reverseWord(input);
		assertEquals(output, "a ab abc");
	}
	
	@Test
	public void test5() {
		String input = "a b c";
		String output = ReverseWord.reverseWord(input);
		assertEquals(output, "c b a");
	}
	
	@Test
	public void test6() {
		String input = "coding is fun";
		String output = ReverseWord.reverseWord(input);
		assertEquals(output, "fun is coding");
	}

}
