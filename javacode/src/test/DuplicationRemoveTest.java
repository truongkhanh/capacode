package test;

import org.junit.Assert;
import org.junit.Test;

import code.classes.SingleLinkedList;
import code.classes.SingleLinkedListNode;
import code.linkedlist.DuplicationRemove;

public class DuplicationRemoveTest {
	enum Strategy {UsingSet, NotUsingSet, Sort};
	Strategy strategy = Strategy.Sort;
	
	SingleLinkedListNode<Integer> removeDuplication(SingleLinkedListNode<Integer> head){
		if (strategy == Strategy.UsingSet){
			return DuplicationRemove.removeDuplication1(head);
		} else if(strategy == Strategy.NotUsingSet){
			return DuplicationRemove.removeDuplication2(head);
		} else{
			return DuplicationRemove.removeDuplication3(head);
		}
	}
	@Test
	public void test1(){
		Integer[] elements = new Integer[]{1, 2, 1, 3, 1, 4, 2,3};
		int size = 4;
		
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		SingleLinkedListNode<Integer> newHead = DuplicationRemove.removeDuplication1(head);
		SingleLinkedList<Integer> newList = new SingleLinkedList<Integer>(newHead);
		
		Assert.assertEquals(list.getAllDistinctValues(), newList.getAllDistinctValues());
		Assert.assertEquals(size, newList.length());
	}
	
	@Test
	public void test2(){
		Integer[] elements = new Integer[]{1, 2, 3};
		int size = 3;
		
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		SingleLinkedListNode<Integer> newHead = DuplicationRemove.removeDuplication1(head);
		SingleLinkedList<Integer> newList = new SingleLinkedList<Integer>(newHead);
		
		Assert.assertEquals(list.getAllDistinctValues(), newList.getAllDistinctValues());
		Assert.assertEquals(size, newList.length());
	}
	
	@Test
	public void test3(){
		Integer[] elements = new Integer[]{1};
		int size = 1;
		
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		SingleLinkedListNode<Integer> newHead = DuplicationRemove.removeDuplication1(head);
		SingleLinkedList<Integer> newList = new SingleLinkedList<Integer>(newHead);
		
		Assert.assertEquals(list.getAllDistinctValues(), newList.getAllDistinctValues());
		Assert.assertEquals(size, newList.length());
	}
	
	@Test
	public void test4(){
		Integer[] elements = new Integer[]{1,1};
		int size = 1;
		
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		SingleLinkedListNode<Integer> newHead = DuplicationRemove.removeDuplication1(head);
		SingleLinkedList<Integer> newList = new SingleLinkedList<Integer>(newHead);
		
		Assert.assertEquals(list.getAllDistinctValues(), newList.getAllDistinctValues());
		Assert.assertEquals(size, newList.length());
	}
	
	@Test
	public void test5(){
		Integer[] elements = new Integer[]{};
		int size = 0;
		
		SingleLinkedList<Integer> list = new SingleLinkedList<Integer>(elements);
		
		SingleLinkedListNode<Integer> head = list.getHead();
		SingleLinkedListNode<Integer> newHead = DuplicationRemove.removeDuplication1(head);
		SingleLinkedList<Integer> newList = new SingleLinkedList<Integer>(newHead);
		
		Assert.assertEquals(list.getAllDistinctValues(), newList.getAllDistinctValues());
		Assert.assertEquals(size, newList.length());
	}
}
