package test;

import org.junit.Assert;
import org.junit.Test;

import code.array.CookingtheBooks;

public class CookingtheBooksTest {

	@Test
	public void test1(){
		char[] number = "1220".toCharArray();
		char[] smallestExpected = "1022".toCharArray();
		
		Assert.assertArrayEquals(smallestExpected, CookingtheBooks.getSmallestNumberCanChange(number));
	}
	
	@Test
	public void test2(){
		char[] number = "2110".toCharArray();
		char[] smallestExpected = "1120".toCharArray();
		
		Assert.assertArrayEquals(smallestExpected, CookingtheBooks.getSmallestNumberCanChange(number));
	}
	
	@Test
	public void test3(){
		char[] number = "2330".toCharArray();
		char[] smallestExpected = "2033".toCharArray();
		
		Assert.assertArrayEquals(smallestExpected, CookingtheBooks.getSmallestNumberCanChange(number));
	}
	
	@Test
	public void test4(){
		char[] number = "0".toCharArray();
		char[] smallestExpected = "0".toCharArray();
		
		Assert.assertArrayEquals(smallestExpected, CookingtheBooks.getSmallestNumberCanChange(number));
	}
	
	@Test
	public void test5(){
		char[] number = "10234".toCharArray();
		char[] smallestExpected = "10234".toCharArray();
		
		Assert.assertArrayEquals(smallestExpected, CookingtheBooks.getSmallestNumberCanChange(number));
	}
}

