package test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import code.classes.SingleLinkedList;

public class SingleLinkedListTest {
	
	@Test
	public void test1(){
		SingleLinkedList<Integer> linkedList = new SingleLinkedList<Integer>(new Integer[]{});
		int length = 0;
		Assert.assertEquals(length, linkedList.length());
		
		Set<Integer> allDistinctValues = new HashSet<>();
		Assert.assertEquals(allDistinctValues, linkedList.getAllDistinctValues());
		
		List<Integer> allValues = new ArrayList<>();
		Assert.assertEquals(allValues, linkedList.getAllValues());
	}
	
	@Test
	public void test2(){
		SingleLinkedList<Integer> linkedList = new SingleLinkedList<Integer>(new Integer[]{1});
		int length = 1;
		Assert.assertEquals(length, linkedList.length());
		
		Set<Integer> allDistinctValues = new HashSet<>();
		allDistinctValues.add(1);
		Assert.assertEquals(allDistinctValues, linkedList.getAllDistinctValues());
		
		List<Integer> allValues = new ArrayList<>();
		allValues.add(1);
		Assert.assertEquals(allValues, linkedList.getAllValues());
	}
	
	@Test
	public void test3(){
		SingleLinkedList<Integer> linkedList = new SingleLinkedList<Integer>(new Integer[]{1, 2});
		int length = 2;
		Assert.assertEquals(length, linkedList.length());
		
		Set<Integer> allDistinctValues = new HashSet<>();
		allDistinctValues.add(1);
		allDistinctValues.add(2);
		Assert.assertEquals(allDistinctValues, linkedList.getAllDistinctValues());
		
		List<Integer> allValues = new ArrayList<>();
		allValues.add(1);
		allValues.add(2);
		Assert.assertEquals(allValues, linkedList.getAllValues());
	}
	
	@Test
	public void test4(){
		SingleLinkedList<Integer> linkedList = new SingleLinkedList<Integer>(new Integer[]{1, 2, 3});
		int length = 3;
		Assert.assertEquals(length, linkedList.length());
		
		Set<Integer> allDistinctValues = new HashSet<>();
		for(int i = 1; i <= 3; i++){
			allDistinctValues.add(i);
		}
		
		Assert.assertEquals(allDistinctValues, linkedList.getAllDistinctValues());
		
		List<Integer> allValues = new ArrayList<>();
		for(int i = 1; i <= 3; i++){
			allValues.add(i);
		}

		Assert.assertEquals(allValues, linkedList.getAllValues());
	}
	
	@Test
	public void test5(){
		SingleLinkedList<Integer> linkedList = new SingleLinkedList<Integer>(new Integer[]{1, 2, 3, 4, 3});
		int length = 5;
		Assert.assertEquals(length, linkedList.length());
		
		Set<Integer> allDistinctValues = new HashSet<>();
		for(int i = 1; i <= 4; i++){
			allDistinctValues.add(i);
		}
		
		Assert.assertEquals(allDistinctValues, linkedList.getAllDistinctValues());
		
		List<Integer> allValues = new ArrayList<>();
		for(int i = 1; i <= 4; i++){
			allValues.add(i);
		}
		allValues.add(3);

		Assert.assertEquals(allValues, linkedList.getAllValues());
	}
	
	
	@Test
	public void test6(){
		SingleLinkedList<Integer> linkedList = new SingleLinkedList<Integer>(new Integer[]{1, 2, 3, 4, 1, 2, 4, 5});
		int length = 8;
		Assert.assertEquals(length, linkedList.length());
		
		Set<Integer> allDistinctValues = new HashSet<>();
		for(int i = 1; i <= 5; i++){
			allDistinctValues.add(i);
		}
		
		Assert.assertEquals(allDistinctValues, linkedList.getAllDistinctValues());
		
		List<Integer> allValues = new ArrayList<>();
		for(int i = 1; i <= 4; i++){
			allValues.add(i);
		}
		allValues.add(1);
		allValues.add(2);
		allValues.add(4);
		allValues.add(5);

		Assert.assertEquals(allValues, linkedList.getAllValues());
	}
}
