package test;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import code.array.HighestProductOfSubSetArray;

public class HighestProductOfSubSetArrayTest {
	@Test
	public void whenAllZero() {
		List<Integer> A = Arrays.asList(0, 0, 0);
		int expected = 0;
		int result = HighestProductOfSubSetArray.compute(A, 3);
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenAllPositives() {
		List<Integer> A = Arrays.asList(1, 2, 3, 4);
		int expected = 24;
		int result = HighestProductOfSubSetArray.compute(A, 3);
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenAllNegatives() {
		List<Integer> A = Arrays.asList(-1, -2, -3, -4);
		int expected = -6;
		int result = HighestProductOfSubSetArray.compute(A, 3);
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenAllNegativesWithZero() {
		List<Integer> A = Arrays.asList(-1, -2, -3, -4, 0);
		int expected = 0;
		int result = HighestProductOfSubSetArray.compute(A, 3);
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenHighestProductFromAllPositives() {
		List<Integer> A = Arrays.asList(10, 20, 30, -5, -6);
		int expected = 6000;
		int result = HighestProductOfSubSetArray.compute(A, 3);
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenHighestProductFromAllNegatives() {
		List<Integer> A = Arrays.asList(10, 20, 30, -50, -60);
		int expected = 3000;
		int result = HighestProductOfSubSetArray.compute(A, 2);
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenHighestProductFromPositiveNegative() {
		List<Integer> A = Arrays.asList(10, 20, 30, -50, -60);
		int expected = 90000;
		int result = HighestProductOfSubSetArray.compute(A, 3);
		Assert.assertEquals(expected, result);
	}
}
