package test;

import org.junit.Assert;
import org.junit.Test;

import counting.SmallestNumberAfterDelete;


public class SmallestNumberAfterDeleteTest {
	
	@Test
	public void whenAllDigitsSame(){
		String input = "111111";
		String expected = "11";
		String result = SmallestNumberAfterDelete.find(input, 4);
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenNoDigitToDelete(){
		String input = "12345";
		String expected = "12345";
		String result = SmallestNumberAfterDelete.find(input, 0);
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void normalCase(){
		String input = "24635";
		String expected = "23";
		String result = SmallestNumberAfterDelete.find(input, 3);
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenIncreasingOrder(){
		String input = "12345";
		String expected = "123";
		String result = SmallestNumberAfterDelete.find(input, 2);
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void whenDecreasingOrder(){
		String input = "54321";
		String expected = "321";
		String result = SmallestNumberAfterDelete.find(input, 2);
		Assert.assertEquals(expected, result);
	}
	
}
