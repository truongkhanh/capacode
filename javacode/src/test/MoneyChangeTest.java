package test;

import static org.junit.Assert.*;

import org.junit.Test;

import code.dynamicprogramming.MoneyChange;



public class MoneyChangeTest {

	@Test
	public void test1(){
		int[] noteValues = new int[]{1,2,5};
		int amount = 3;
		int numWays = MoneyChange.numWaysChange(amount, noteValues);
		assertEquals(2, numWays);
	}
	
	@Test
	public void test2(){
		int[] noteValues = new int[]{1,2,5};
		int amount = 8;
		int numWays = MoneyChange.numWaysChange(amount, noteValues);
		assertEquals(7, numWays);
	}
	
	@Test
	public void test3(){
		int[] noteValues = new int[]{1,2,5};
		int amount = 1000;
		int numWays = MoneyChange.numWaysChange(amount, noteValues);
		assertEquals(50401, numWays);
	}
	
	@Test
	public void test4(){
		int[] noteValues = new int[]{1,2,5};
		int amount = 4;
		int numWays = MoneyChange.numWaysChange(amount, noteValues);
		assertEquals(3, numWays);
	}
	
	@Test
	public void test5(){
		int[] noteValues = new int[]{1,2,5};
		int amount = 1;
		int numWays = MoneyChange.numWaysChange(amount, noteValues);
		assertEquals(1, numWays);
	}
	
	@Test
	public void test6(){
		int[] noteValues = new int[]{1,2,5};
		int amount = 16;
		int numWays = MoneyChange.numWaysChange(amount, noteValues);
		assertEquals(20, numWays);
	}
	
	@Test
	public void test7(){
		int[] noteValues = new int[]{5, 2, 1};
		int amount = 1000;
		int numWays = MoneyChange.numWaysChange(amount, noteValues);
		assertEquals(50401, numWays);
	}
	
	@Test
	public void test8(){
		int[] noteValues = new int[]{2, 1, 5};
		int amount = 16;
		int numWays = MoneyChange.numWaysChange(amount, noteValues);
		assertEquals(20, numWays);
	}
	
	@Test
	public void test9(){
		int[] noteValues = new int[]{2, 1, 5};
		int amount = 8;
		int numWays = MoneyChange.numWaysChange(amount, noteValues);
		assertEquals(7, numWays);
	}
}
