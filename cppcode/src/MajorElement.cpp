#include <iostream>

using namespace std;
//int A[] = {3,6,3,6,3,6,3,6,3};
int A[] = {1, 2, 3, 4, 5, 5,5,5,5,5};
int sz = sizeof(A)/sizeof(int);
int N = sz / 2;

int main()
{
    int candidate = A[0];
    int vote = 1;
    for(int k = 1; k<sz; k++){
        if (candidate == A[k]){
            vote ++; //up-vote
            continue;
        };
        if (vote > 0){
            vote --; //down-vote
            continue;
        }else{// new candidate
            candidate = A[k];
            vote = 1;
        }
    };
    //double check for candidate
    vote = 0;
    for(int k = 0; k<sz; k++) vote += (candidate == A[k]);

    if (vote > N) cout <<"Major element is: "<<candidate<<endl;
    else cout <<"There's no major element!"<<endl;

    return 0;
}
