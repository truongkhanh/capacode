#include <iostream>
#include <algorithm>

using namespace std;
const int NMAX = 100;
const int MAX_INT = 1<<30;
int A[] = {10, 8, 6, 10, 2, 10, 6, 14, 4, 9, 5, 13, 3, 11, 7, 15, 80, 9, 10, 10,10,10};
int N = sizeof(A)/sizeof(int);
int LEOS[NMAX], LISS[NMAX];
//-----------------------------------------------

int findLargestIndex(int (&Arr)[NMAX], int x){
    //for Longest NON-Decreasing Subsequence:
    int res = upper_bound(Arr, Arr+N, x) - Arr;
    //for Longest Increasing Subsequence:
    //int res = lower_bound(Arr, Arr+N, x) - Arr;
    return res;
};
//-----------------------------------------------

void LIS(){
    //initialize LEOS:
    for(int x = 0; x <NMAX; x++)
        LEOS[x] = +MAX_INT;

    //initialize:
    LEOS[0] = -MAX_INT;
    LEOS[1] = A[0];
    LISS[0] = 1;

    //dynamic programming:
    for(int k=1; k<N; k++){
        //find the best LIS to append A[k] to:
        int w = findLargestIndex(LEOS, A[k]) -1;
        //w is the largest index of LEOS that LEOS[w] < A[k];
        //Note: w is the length of LIS that ends at LEOS[w].

        //A[k] can be appended into the LIS that ends at LEOS[w]:
        LISS[k] = w + 1;

        //store index for reconstruction of LIS:
        //Prev[k] = INDX[w];

        //update LEOS:
        if (LEOS[w+1] > A[k]){
            LEOS[w+1] = A[k];
            //INDX[w+1] = k;
        }
    };
};
//-----------------------------------------------

void output(){
    //locate the maximum LIS size:
    int Longest = 0, id = 0;
    for(int k = 0; k<N; k++)
    if (    Longest <  LISS[k]
        ||  Longest == LISS[k] && A[id] > A[k]
        ){
        Longest = LISS[k];
        id = k;
    }

    //reconstruct the LIS:
    int LIS[NMAX];
    int cnt = Longest;
    LIS[cnt+1] = MAX_INT;
    for(int i = N-1; i>0; i--)
    if (LISS[i] == cnt){
        //for Longest non-decreasing subsequence:
        if (A[i]<=LIS[cnt+1])
        //for Longest strictly increasing subsequence:
        //if (A[i]<LIS[cnt+1])
            LIS[cnt--] = A[i];
    }

    //output:
    cout <<"Length of the LIS is: "<<Longest<<endl;
    cout <<"LIS is: ";
    for(int i=1; i<=Longest; i++) cout <<LIS[i]<<" ";
    cout <<endl;
};
//-----------------------------------------------

int main()
{
    LIS();
    output();
    return 0;
}
