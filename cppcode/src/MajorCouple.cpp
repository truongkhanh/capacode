#include <iostream>

using namespace std;
int A[] = {1, 2, 3, 4, 5, 5, 5, 5, 3, 3};
int sz = sizeof(A)/sizeof(int);
int N = sz / 3;

int main()
{
    int x = A[0];
    int xVote = 1;
    int n = 1;
    while(A[n] == x){
        xVote++;
        n++;
    };

    int y = A[n];
    int yVote = 1;
    while(A[n] == y){
        yVote++;
        n++;
    };

    for(int k = n; k<sz; k++){
        //up-vote:
        if (A[k] == x){ xVote++; continue;}
        if (A[k] == y){ yVote++; continue;}
        //down-vote both:
        if (xVote >0 && yVote >0 ){
            xVote--;
            yVote--;
            continue;
        };
        //propose new candidate:
        if (xVote ==0 ){
            x = A[k];
            xVote++;
            continue;
        };
        if (yVote ==0 ){
            y = A[k];
            yVote++;
            continue;
        }

    }

    //double check for candidate
    xVote = 0, yVote = 0;
    for(int k = 0; k<sz; k++){
        xVote += (x == A[k]);
        yVote += (y == A[k]);
    }

    if (xVote > N and yVote > N) cout <<"Major couple is: "<<x<<" and "<<y<<endl;
    else cout <<"There's no major couple!"<<endl;

    return 0;
}
