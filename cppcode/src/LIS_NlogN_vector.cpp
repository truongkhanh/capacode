#include <iostream>
#include <algorithm>

using namespace std;
int A[] = {10, 8, 6, 10, 2, 10, 6, 14, 4, 9, 5, 13, 3, 11, 7, 15, 80, 9, 10, 10,10,10};
int N = sizeof(A)/sizeof(int);
vector<int> LEOS;
vector<int>::iterator it;
//-----------------------------------------------

// Change upper_bound() into lower_bound() to get strictly increasing subsquence!
void LISS(){
    for(int k=0; k<N; k++){
        it = upper_bound(LEOS.begin(), LEOS.end(), A[k]);
        if (it == LEOS.end()) LEOS.push_back(A[k]);
        else *it = A[k];
    };
    cout <<"Length of the LIS is: "<<LEOS.size()<<endl;
};
//-----------------------------------------------

int main(){
    LISS();
    return 0;
}
