/*
CS3230 : Design and Analysis of Algorithms (Fall 2014)
Tutorial Set #4
[For discussion during Week 6]

D4. [Median of two sorted arrays] (Modified from past Mid-Term Quiz)
(a)
If you are given the following sorted arrays:
A = [ 3, 7, 22, 29, 31, 53, 67, 78 ]
B = [ 5, 11, 13, 17, 23, 37, 43, 97 ]
What is the 8th smallest element (median) in the union of the two sorted arrays?

(b) [Finding nth smallest in two sorted arrays , each of size n]
You are given two sorted arrays, A[1..n] and B[1..n], both of size n. You can
assume that the two arrays are sorted in increasing order and that the elements
are all distinct (there are no repeated elements). Give an O(lg n) time
algorithm for computing the median (nth smallest element) in the union of the
two arrays


Implemented by Nguyen NN (nguyennn@comp.nus.edu.sg)
*/

#include <iostream>
#include <algorithm>

using namespace std;

const int Nmax = 200000;
int A[Nmax], B[Nmax];
int n;
int nCalls = 0;


void printArrays(int startA, int endA, int startB, int endB){
    int midA = (endA+startA)/2;
    int midB = (endB+startB)/2;
    cout <<"Array A: \t";
    for (int i = 0; i<startA; i++) cout <<".\t";
    for (int i = startA; i<=endA; i++){
        if (i == midA) cout <<"(";
        cout <<A[i];
        if (i == midA) cout <<")";
        cout <<"\t";
    };
    for (int i = endA+1; i<n; i++) cout <<".\t";
    cout <<endl;

    cout <<"Array B: \t";
    for (int i = 0; i<startB; i++) cout <<".\t";
    for (int i = startB; i<=endB; i++){
        if (i == midB) cout <<"(";
        cout <<B[i];
        if (i == midB) cout <<")";
        cout <<"\t";
    };
    for (int i = endB+1; i<n; i++) cout <<".\t";
    cout <<endl;
};


int trivialCase(int startA, int endA, int startB, int endB){
    int tmp[4];
    tmp[0] = A[startA]; tmp[1] = A[endA];
    tmp[2] = B[startB]; tmp[3] = B[endB];
    sort(tmp, tmp+4);
    cout <<"Trivial case: Median must be "<<tmp[1]<<endl;
    return tmp[1];
};


int median(int startA, int endA, int startB, int endB){
    nCalls++;
    printArrays(startA, endA, startB, endB);

    //init. case:
    if (endA - startA <2){
        return trivialCase(startA, endA, startB, endB);
    };

    //recursive:
    int midA = (endA+startA)/2;
    int midB = (endB+startB)/2;
    int distance = midA - startA;
    if (distance < endA - midA) distance = endA - midA;

    cout <<"Compare "<<A[midA] <<" and " << B[midB]<<":"<<endl;

    if (A[midA] < B[midB]){
        cout <<"Eliminated front half of A and back half of B"<<endl<<endl;
        return median(midA, midA + distance, startB, startB + distance);
    }
    else {
        cout <<"Eliminated front half of B and back half of A"<<endl<<endl;
        return median(startA, startA + distance, midB, midB + distance);
    };

};


void input1(){
    A = {3, 7, 22, 29, 31, 53, 67, 78 , 100, 1000};
    B = {5, 11, 13, 17, 23, 37, 43, 97, 101, 999};
    n = 8;
}


void input2(char *nn){
    n = atoi(nn);//n = 20;
    for(int i = 0; i<n; i++){
        A[i] = i*2+1;
        B[i] = A[i]+1;
    }
}


int main(int argc, char *argv[]){

    input1();
    if (argc >1){
        input2(argv[1]);
    }

    int res = median(0,n-1,0,n-1);

    cout <<endl;
    cout <<"Median of the two arrays is: "<<res<<endl;
    cout <<"Number of function calls is: "<<nCalls<<endl;

    return 0;
}

/*

>median
Array A: 	3	7	22	(29)	31	53	67	78
Array B: 	5	11	13	(17)	23	37	43	97
Compare 29 and 17:
Eliminated front half of B and back half of A

Array A: 	3	7	(22)	29	31	.	.	.
Array B: 	.	.	.	17	23	(37)	43	97
Compare 22 and 37:
Eliminated front half of A and back half of B

Array A: 	.	.	22	(29)	31	.	.	.
Array B: 	.	.	.	17	(23)	37	.	.
Compare 29 and 23:
Eliminated front half of B and back half of A

Array A: 	.	.	(22)	29	.	.	.	.
Array B: 	.	.	.	.	(23)	37	.	.
Trivial case: Median must be 23

Median of the two arrays is: 23
Number of function calls is: 4


>median 13
Array A: 	1	3	5	7	9	11	(13)	15	17	19	21	23	25
Array B: 	2	4	6	8	10	12	(14)	16	18	20	22	24	26
Compare 13 and 14:
Eliminated front half of A and back half of B

Array A: 	.	.	.	.	.	.	13	15	17	(19)	21	23	25
Array B: 	2	4	6	(8)	10	12	14	.	.	.	.	.	.
Compare 19 and 8:
Eliminated front half of B and back half of A

Array A: 	.	.	.	.	.	.	13	(15)	17	19	.	.	.
Array B: 	.	.	.	8	(10)	12	14	.	.	.	.	.	.
Compare 15 and 10:
Eliminated front half of B and back half of A

Array A: 	.	.	.	.	.	.	13	(15)	17	.	.	.	.
Array B: 	.	.	.	.	10	(12)	14	.	.	.	.	.	.
Compare 15 and 12:
Eliminated front half of B and back half of A

Array A: 	.	.	.	.	.	.	(13)	15	.	.	.	.	.
Array B: 	.	.	.	.	.	(12)	14	.	.	.	.	.	.
Trivial case: Median must be 13

Median of the two arrays is: 13
Number of function calls is: 5
*/
