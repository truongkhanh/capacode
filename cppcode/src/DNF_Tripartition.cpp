#include <iostream>
#include <algorithm>

using namespace std;

const int red = 0, white = 1, blue = 2;
const string color[] = {"red", "white", "blue"};

int A[] = {red, blue, white, white, red, blue, white, red, blue};
int n = sizeof(A) / sizeof(int);


void printArr(){
    for(int i =0; i<n; i++) cout <<color[A[i]]<<"\t";
    cout <<endl;
};

void Tripartition(){
    int r = 0, w = 0, b = n-1;
    while(w <=b){
        if (A[w] == red){   // A[w] < pv1
            swap(A[r],A[w]);
            r++;
            w++;
        }
        else if (A[w] == blue){ // A[w] > pv2
            swap(A[w],A[b]);
            b--;
        }
        else  w++;
    };
};


int main()
{
    printArr();
    Tripartition();
    printArr();
    return 0;
}
