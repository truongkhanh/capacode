///////////////////////////////////////////////////////////////////////////////
//
//  Problem:    K Most Frequence Items
//              (Question#2 @ https://evernote.com/careers/challenge.php)
//https://dl.dropboxusercontent.com/u/39463358/WWW/EvernoteCodingChallenge.html
//
//  Idea:       Use a hash to count the frequency of each term;
//              After that, use a MIN HEAP priority queue of size K to store
//              the most frequent items. Note: have to use min heap!
//
//  Complexity: O(max(N, M logK)) time and O(M) space,
//              where: N = size of input; M = number of different terms;
//              Note: using C++ map instead of unordered_map: O(N logM)) time.
//
//  Author:     Nguyen Nam Ninh, comp.nus.edu.sg/~nguyennn
//              Email: ninhnnsoc@gmail.com
//  Date:       Nov, 2014
//
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <map>
#include <tr1/unordered_map>    // support since C++11
#include <vector>
#include <queue>

using namespace std;

typedef     string      term_Type;
typedef     int         frequency_Type;
typedef     pair<term_Type,frequency_Type>       PSI;   // Pair of String and Int

struct MinPQComparator{ // for min heap priority queue!
    // term frequency descending, then term lexicographical order:
    bool operator() (const PSI &lhs, const PSI &rhs) const {
        if (lhs.second >  rhs.second) return true;
        if (lhs.second <  rhs.second) return false;
        if (lhs.second == rhs.second) return (lhs.first < rhs.first);
    };
};

typedef     priority_queue <PSI, vector<PSI >, MinPQComparator> myMinPQ_Type;

typedef     map<term_Type, frequency_Type> myMap_Type;
//typedef tr1::unordered_map<term_Type, frequency_Type> myMap_Type;

//=============================================================================

myMap_Type      FrequencyOf;
myMinPQ_Type    LeastFrequent;

int N;
int K;

//=============================================================================

void readInput(){
    cin >> N;
    string aTerm;
    for(int i = 0; i<N; i++){   //  O(N)
        cin >> aTerm;
        FrequencyOf[aTerm]++;   // O(1) or O(log M) depends on map type;
    };
    cin >> K;
};
//-----------------------------------------------------------------------------

void solveIt(){
    map<string, int>::iterator it;
    //tr1::unordered_map<string, int>::iterator it;

    // queueing into min pq:
    for(it = FrequencyOf.begin(); it != FrequencyOf.end(); ++it){   // O(M)
        PSI aPair(it->first, it->second);

        LeastFrequent.push(aPair); // queued. O(log K)

        if (LeastFrequent.size() > K){
            // surely the top is NOT one of K most frequent items! Discard it.
            LeastFrequent.pop();    // O(log K)
        }
    };// for it
};
//-----------------------------------------------------------------------------

void printResult(){
    vector<PSI> myStack;

    while(!LeastFrequent.empty()){
        myStack.push_back(LeastFrequent.top());
        LeastFrequent.pop();
    };

    //print reverse order: most frequent first
    for(int i = myStack.size()-1; i>=0; i--){
        term_Type aTerm = myStack[i].first;
        cout << aTerm << endl;
    }

};
//=============================================================================

int main()
{
    readInput();
    solveIt();
    printResult();
    return 0;
}
//END
